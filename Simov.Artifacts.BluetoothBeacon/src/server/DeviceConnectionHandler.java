package server;


import business.ArtifactsRepository;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.microedition.io.StreamConnection;
import model.Artifact;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Admin
 */
public class DeviceConnectionHandler implements Runnable{
    
    /**
     * Stream connection
     */
    private StreamConnection streamConnection;
    
    /**
     * Connected device bluetooth address
     */
    private String bluetoothAddress;
    
    /**
     * Connected device friendly name
     */
    private String friendlyName;

    /**
     * Constructor
     * @param connection
     * @param bluetoothAddress
     * @param friendlyName
     */
    public DeviceConnectionHandler(StreamConnection connection, String bluetoothAddress, String friendlyName){
        this.streamConnection = connection;
        this.bluetoothAddress = bluetoothAddress;
        this.friendlyName = friendlyName;
    }

    @Override
    public void run() {
        boolean clientEndedConnection = false;
        
        System.out.println("\n=== Connected to:");
        System.out.println("Remote device address: "+this.bluetoothAddress);
        System.out.println("Remote device name: "+this.friendlyName);
        System.out.println("=================\n");
        
        //create buffer io handlers
        BufferedReader bufferReader = null;
        BufferedWriter bufferWriter = null;
        try{
            // prepare to receive data
            InputStream inputStream = this.streamConnection.openInputStream();
            OutputStream outputStream = this.streamConnection.openOutputStream();

            //create buffer reader/writer 
            bufferReader = new BufferedReader(new InputStreamReader(inputStream));
            bufferWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
        }catch(Exception ex){
            System.out.println("====== Disconected from device\n\n");
            return;
        }
                
        try {     
            
            //====== GAIN ACCESS TO REPOSITORY
            
            ArtifactsRepository repository = ArtifactsRepository.attach(this.friendlyName);
        
            if(repository == null){
                //notify that connection will be closed because the data is not available at the moment
                this.send(bufferWriter,CommunicationProtocol.DATABASE_UNAVAILABLE_MESSAGE);
                
                throw new Exception("= REPOSITORY UNAVAILABLE =");
            }else{
                //notify that connection will procced and the data is available
                this.send(bufferWriter,CommunicationProtocol.DATABASE_AVAILABLE_MESSAGE);
            }

            //====== proceed to synchronization
            
            
            //============================
            //=== Receive new Artifacts
            //============================

            String message;
            
            this.expectMessage(bufferReader, CommunicationProtocol.NEW_ARTIFACTS_BEGIN_MESSAGE,true);
            List<Integer> newArtifactsIds = new ArrayList<>();
            
            //receive new Artifacts
            boolean finish = false;
            do{

                //read artifact
                message = this.receiveMessage(bufferReader);

                if(message.startsWith(CommunicationProtocol.NEW_ARTIFACT_MESSAGE)){
                    
                    //get artifact data
                    String user = this.getMessageProperty(message, 0);
                    String title = this.getMessageProperty(message, 1);
                    String description = this.getMessageProperty(message, 2);
                    String latitude = this.getMessageProperty(message, 3);
                    String longitude = this.getMessageProperty(message, 4);

                    //store artifact in database
                    int id = repository.newArtifact(user, title, description, latitude, longitude);

                    //send id
                    if(id != -1){
                        this.send(bufferWriter, CommunicationProtocol.NEW_ARTIFACT_ID_MESSAGE + id);
                        newArtifactsIds.add(id);
                    }else{
                        this.send(bufferWriter, CommunicationProtocol.NEW_ARTIFACT_ERROR);
                    }
                    
                }else if(CommunicationProtocol.NEW_ARTIFACTS_END_MESSAGE.equals(message)) {
                    finish = true;
                }else{
                    throw new Exception("= UNEXPECTED MESSAGE: "+message);
                }
                
            }while(finish == false);
            
           
            //============================
            //=== Send Artifacts
            //============================
            
            //begin
            this.send(bufferWriter, CommunicationProtocol.SERVER_ARTIFACTS_BEGIN_MESSAGE);

            //send all artifacts one by one
            List<Artifact> artifacts = repository.getAllArtifacts();
            for(Artifact artifact : artifacts){
                
                if(!newArtifactsIds.contains(artifact.id)){
                    this.send(bufferWriter, CommunicationProtocol.SERVER_ARTIFACT_MESSAGE
                            + artifact.id + CommunicationProtocol.SEPARATOR_TOKEN
                            + artifact.userId + CommunicationProtocol.SEPARATOR_TOKEN
                            + artifact.title + CommunicationProtocol.SEPARATOR_TOKEN
                            + artifact.description + CommunicationProtocol.SEPARATOR_TOKEN
                            + artifact.latitude + CommunicationProtocol.SEPARATOR_TOKEN
                            + artifact.longitude);
                }
            }

            //end
            this.send(bufferWriter, CommunicationProtocol.SERVER_ARTIFACTS_END_MESSAGE);
            
            //============================
            //=== End connection
            //============================
            
            //wait client connection close
            this.expectMessage(bufferReader,CommunicationProtocol.APP_END_CONNECTION_MESSAGE,true);

        } catch (EndConnectionException ex) {
            clientEndedConnection = true;
        } catch (Exception e) {
            System.err.println("ERROR: "+e.getMessage());

        }
        
        try {
            if(clientEndedConnection == false){
                this.send(bufferWriter, CommunicationProtocol.SERVER_END_CONNECTION_MESSAGE);
            }
            ArtifactsRepository.release();
            bufferWriter.close();
            bufferReader.close();
        } catch (Exception ex) {
        }
        System.out.println("\n====== Disconected from device\n\n");
    }
    
    /**
     * Wait for a message from client
     * @param bufferReader
     * @return
     * @throws Exception 
     */
    private String receiveMessage(BufferedReader bufferReader) throws Exception{
        String lineRead = this.readUntilCharacter(bufferReader,CommunicationProtocol.MESSAGE_DELIMITER_TOKEN);
        System.out.println("Client '"+this.friendlyName+"' : "+lineRead);
        
        if(lineRead.equals(CommunicationProtocol.APP_END_CONNECTION_MESSAGE)){
            throw new EndConnectionException();
        }
        
        return lineRead;
    }
    
    /**
     * Wair fot a specific message from client
     */
    private String expectMessage(BufferedReader bufferReader, String message, boolean exactMessage) throws Exception{
        String lineRead = this.readUntilCharacter(bufferReader,CommunicationProtocol.MESSAGE_DELIMITER_TOKEN);
        System.out.println("Client '"+this.friendlyName+"' : "+lineRead);
        
        if(lineRead.equals(CommunicationProtocol.APP_END_CONNECTION_MESSAGE)){
            throw new EndConnectionException();
        }

        if(exactMessage == true){
            if(message.equals(lineRead)){
                return message;
            }
        }else{
            if(lineRead.startsWith(message)){
                return lineRead;
            }
        }
        
        throw new Exception(" UNEXPECTED MESSAGE: "+lineRead);
    }
  
    /**
     * Send message to client
     */
    private void send(BufferedWriter writer, String message) throws Exception{
        writer.write(message);
        writer.write(CommunicationProtocol.MESSAGE_DELIMITER_TOKEN);
        writer.flush();
        System.out.println("Server : "+message);
    }
    
    /**
     * Get property from a complex message
     * @param message
     * @param index
     * @return 
     */
    public String getMessageProperty(String message, int index){
        String[] messageMainParts = message.split(CommunicationProtocol.MESSAGE_VALUE_DELIMITER_TOKEN);
        
        if(messageMainParts.length != 2){
            return null;
        }
        
        String[] messageParts = messageMainParts[1].split(CommunicationProtocol.SEPARATOR_TOKEN);
        
        if(messageParts.length > index){
            return messageParts[index];
        }
        
        return null;
    }
    
    /**
     * Reads from buffer until a certain character is readed
     */
    private String readUntilCharacter(BufferedReader bufferReader, char character) throws IOException{
        StringBuilder builder = new StringBuilder();
        char characterReaded = '0';
        
        do{
            characterReaded = (char)bufferReader.read();
            if(characterReaded != character){
                builder.append(characterReaded);
            }
        }while(characterReaded != character);
        
        return builder.toString();
    }
    
    /**
     * EndConnectionException
     */
    private class EndConnectionException extends Exception{
        public EndConnectionException(){
            super("= CLIENT FINISHED THE CONNECTION =");
        }
    }
}
