package server;


import database.LocalStorageDatabase;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;
import server.Server;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Admin
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String filename = "data.xml";
        
        try {
            //create database
            LocalStorageDatabase database = new LocalStorageDatabase(filename);
            
            //create server
            Server server = new Server("Simov_Artifacts_BluetoothBeacon",database);

            //run server
            Thread serverThread = new Thread(server);
            serverThread.start();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
