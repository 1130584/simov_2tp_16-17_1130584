/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.util.UUID;

/**
 * Artifacts syncronization communication Protocol tokens, codes and directives
 * @author Admin
 */
public class CommunicationProtocol {
    
    /**
     * Pre-shared token to authenticate the connection with the android app.
     * This lets the android app know that it is communicating with an official 'Artifacts' bluetooth beacon
     *
     * It is used as bluetooth service UUID to authenticate in the connection phase
     */
    public static final String ARTIFACTS_SYNC_BEACON_UUID_STRING = "c1aaa0e0-d2d9-11e6-9598-0800200c9a66";
    
    //message directives
    public static final String MESSAGE_BODY_DELIMITER_TOKEN = ":";
    public static final String SEPARATOR_TOKEN = ";";
    public static final String MESSAGE_VALUE_DELIMITER_TOKEN = "=";
    public static final char MESSAGE_DELIMITER_TOKEN = '#';
    public static final String APP_MESSAGE_DIRECTIVE = "simov.artifacts.androidapp";
    public static final String SERVER_MESSAGE_DIRECTIVE = "simov.artifacts.bluetoothbeacon";

    //===== CLIENT

    //connection protocol messages
    public static final String APP_END_CONNECTION_MESSAGE = APP_MESSAGE_DIRECTIVE + MESSAGE_BODY_DELIMITER_TOKEN + "end_connection";

    //client artifacts synchronization messages
    public static final String NEW_ARTIFACTS_BEGIN_MESSAGE = APP_MESSAGE_DIRECTIVE + MESSAGE_BODY_DELIMITER_TOKEN + "new_artifacts_begin";
    public static final String NEW_ARTIFACT_MESSAGE = APP_MESSAGE_DIRECTIVE + MESSAGE_BODY_DELIMITER_TOKEN + "new_artifact"+MESSAGE_VALUE_DELIMITER_TOKEN;
    public static final String NEW_ARTIFACTS_END_MESSAGE = APP_MESSAGE_DIRECTIVE + MESSAGE_BODY_DELIMITER_TOKEN + "new_artifacts_end";

    //===== SERVER

    //connection protocol messages
    public static final String SERVER_END_CONNECTION_MESSAGE = SERVER_MESSAGE_DIRECTIVE + MESSAGE_BODY_DELIMITER_TOKEN + "end_connection";
    public static final String DATABASE_AVAILABLE_MESSAGE = SERVER_MESSAGE_DIRECTIVE + MESSAGE_BODY_DELIMITER_TOKEN + "database"+MESSAGE_VALUE_DELIMITER_TOKEN+"available";
    public static final String DATABASE_UNAVAILABLE_MESSAGE = SERVER_MESSAGE_DIRECTIVE + MESSAGE_BODY_DELIMITER_TOKEN + "database"+MESSAGE_VALUE_DELIMITER_TOKEN+"unavailable";

    //client artifacts synchronization messages
    public static final String NEW_ARTIFACT_ID_MESSAGE = SERVER_MESSAGE_DIRECTIVE + MESSAGE_BODY_DELIMITER_TOKEN + "new_artifact_id"+MESSAGE_VALUE_DELIMITER_TOKEN;
    public static final String NEW_ARTIFACT_ERROR = SERVER_MESSAGE_DIRECTIVE + MESSAGE_BODY_DELIMITER_TOKEN + "new_artifact_error";

    //server artifacts synchronization messages
    public static final String SERVER_ARTIFACTS_BEGIN_MESSAGE = SERVER_MESSAGE_DIRECTIVE + MESSAGE_BODY_DELIMITER_TOKEN + "server_artifacts_begin";
    public static final String SERVER_ARTIFACT_MESSAGE = SERVER_MESSAGE_DIRECTIVE + MESSAGE_BODY_DELIMITER_TOKEN + "server_artifact"+MESSAGE_VALUE_DELIMITER_TOKEN;
    public static final String SERVER_ARTIFACTS_END_MESSAGE = SERVER_MESSAGE_DIRECTIVE + MESSAGE_BODY_DELIMITER_TOKEN + "server_artifacts_end";
    
}
