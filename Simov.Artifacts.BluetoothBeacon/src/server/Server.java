package server;

import business.ArtifactsRepository;
import database.ArtifactsDatabase;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.bluetooth.UUID;

import javax.bluetooth.*;
import javax.microedition.io.*;

/**
* SPP Server
*/
public class Server implements Runnable {
    
    /**
     * Name of server
     */
    private String name;
    
    /** 
     * Constructor 
     * @param name
     * @param database
     * @throws java.lang.Exception
     */
    public Server(String name, ArtifactsDatabase database) throws Exception {
        this.name = name;
        
        //init repository
        ArtifactsRepository.init(database);
    }
    
    @Override
    public void run() {
        LocalDevice localDevice = null;
        StreamConnectionNotifier notifier = null;
        StreamConnection connection = null;

        // setup the server to listen for connections
        try {
            // retrieve the local Bluetooth device object
            localDevice = LocalDevice.getLocalDevice();
            localDevice.setDiscoverable(DiscoveryAgent.GIAC);

            String uuidString = CommunicationProtocol.ARTIFACTS_SYNC_BEACON_UUID_STRING;
            uuidString = uuidString.replace("-", "");
            UUID uuid = new UUID(uuidString, false);
            
            //Create the service url
            String connectionString = "btspp://localhost:" + uuid.toString() +";name="+this.name;
            notifier = (StreamConnectionNotifier)Connector.open(connectionString);
            
            System.out.println("=== Server started");
            System.out.println("Address: "+localDevice.getBluetoothAddress());
            System.out.println("Name: "+localDevice.getFriendlyName());
            System.out.println("==================");
            
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        
        System.out.println("waiting for connections...");
        
        // wait for connection
        while(true) {
            try {
                
                //wait for connection
                //accept connection attemps from other devices
                connection = notifier.acceptAndOpen();

                //create device connection handler
                RemoteDevice dev = RemoteDevice.getRemoteDevice(connection);
                
                String bluetoothAddress = dev.getBluetoothAddress();
                String friendlyName = dev.getFriendlyName(true);
                
                DeviceConnectionHandler clientConnection = new DeviceConnectionHandler(connection,bluetoothAddress,friendlyName);
                
                //Run the client connection handler
                Thread processThread = new Thread(clientConnection);
                processThread.start();
           
            } catch (Exception e) {
                e.printStackTrace();
                break;
            }
        }
    }
}
