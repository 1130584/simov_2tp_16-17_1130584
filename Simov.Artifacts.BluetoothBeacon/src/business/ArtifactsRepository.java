/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import database.ArtifactsDatabase;
import java.util.List;
import model.Artifact;

/**
 * Class ArtifactsRepository
 * @author Admin
 */
public class ArtifactsRepository {
    
    // ========= Singleton
    
    /**
     * Id of the thread that owns the repository
     */
    private static String currentOwnerThreadId = "";
    
    /**
     * Singleton instance
     */
    private static ArtifactsRepository instance;
    
    /**
     * Create singleton instance
     * @param database the database
     * @throws java.lang.Exception
     */
    public static void init(ArtifactsDatabase database) throws Exception{
        if(instance == null){
            instance = new ArtifactsRepository(database);
        }
    }
    
    /**
     * Get repository instance.
     * As the server is multithreaded (one thread per connection)
     * The repository must be used by one at a time that needs to invoke this method to have access to it
     * to avoid data inconsistency
     * @param threadId
     * @return 
     */
    public static synchronized ArtifactsRepository attach(String threadId) throws Exception{
        if(!currentOwnerThreadId.equalsIgnoreCase(threadId)){
            currentOwnerThreadId = threadId;
            instance.open();
            return instance;
        }
        return null;
    }
    
    /**
     * Release repository to allow other thread to use it
     */
    public static void release() throws Exception{
        currentOwnerThreadId = "";
        instance.close();
    }
    
    // =========
    
     /**
     * Artifacts Database
     */
    private ArtifactsDatabase database;

    /**
     * Constructor
     * @param database
     */
    public ArtifactsRepository(ArtifactsDatabase database){
        this.database = database;
    }
    
    /**
     * Insert a new Artifact in database
     * @param userId
     * @param title
     * @param description
     * @param latitude
     * @param longitude
     * @return
     */
    public int newArtifact(String userId, String title, String description, String latitude, String longitude){
        return this.database.newArtifact(userId, title, description, latitude, longitude);
    }
    
    /**
     * Get all artifacts
     * @return
     */
    public List<Artifact> getAllArtifacts(){
        return this.database.getAllArtifacts();
    }
    
    /**
     * Open repository
     * @throws java.lang.Exception
     */
    private void open() throws Exception{
        this.database.open();
    }
    
    /**
     * Close repository
     * @throws Exception 
     */
    private void close() throws Exception{
        this.database.close();
    }
    
}
