/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.util.List;
import model.Artifact;

/**
 * Interface ArtifactsDatabase
 * @author Admin
 */
public interface ArtifactsDatabase {
    
    /**
     * Open database and load data
     * @throws java.lang.Exception
     */
    public void open() throws Exception;
    
    /**
     * Close database and save data
     * @throws java.lang.Exception
     */
    public void close() throws Exception;

    /**
     * Insert a new Artifact in database
     * @param userId
     * @param title
     * @param description
     * @param latitude
     * @param longitude
     * @return
     */
    public int newArtifact(String userId, String title, String description, String latitude, String longitude);

    /**
     * Retrieves an Artifact from database by its id
     * @param id
     * @return
     */
    public Artifact getArtifact(int id);

    /**
     * Get all artifacts count
     * @return
     */
    public int getAllArtifactsCount();

    /**
     * Get all artifacts
     * @return
     */
    public List<Artifact> getAllArtifacts();
}
