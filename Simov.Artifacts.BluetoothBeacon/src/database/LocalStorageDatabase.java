/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBException;
import model.Artifact;

/**
 * Local storage database implementation
 * @author Admin
 */
public class LocalStorageDatabase implements ArtifactsDatabase{
    
    /**
     * Localstorage manager
     */
    private final LocalStorageManager localStorageManager;
    
    /**
     * Data
     */
    private Map<Integer, Artifact> artifacts;
    
    /**
    * File name
    */
    private final String filename;
    
    /**
     * Constructor
     * @param filename file with the data
     * @throws java.lang.Exception
     */
    public LocalStorageDatabase(String filename) throws Exception{
        this.artifacts = new HashMap<>();
        this.filename = filename;
        this.localStorageManager = new LocalStorageManager(filename);
    }
    
    /**
     * Generates a new id 
     */
    private int generateId(){
        return 100000 + this.artifacts.size() + (int)(Math.random() * 1000);
    }
    
    /**
     * Open database and load data
     * @throws java.lang.Exception
     */
    @Override
    public void open() throws Exception{
        List<Artifact> data = this.localStorageManager.load();
        
        this.artifacts = new HashMap<>();
        for(Artifact artifact : data){
            
            if(this.artifacts.get(artifact.id) != null){
                System.out.println("Error reading from file '"+this.filename+"' :\nArtifact with id='"+artifact.id+"' already exists in database");
            }else{
                this.artifacts.put(artifact.id, artifact);
            }
        }
    }
    
    /**
     * Close database and save data
     * @throws java.lang.Exception
     */
    @Override
    public void close() throws Exception{
        List<Artifact> allArtifacts = new ArrayList<>(this.artifacts.values());
        this.localStorageManager.save(allArtifacts);
    }

    /**
     * Insert a new Artifact in database
     * @param userId
     * @param title
     * @param description
     * @param latitude
     * @param longitude
     * @return
     */
    @Override
    public int newArtifact(String userId, String title, String description, String latitude, String longitude) {
        //generate new primary key
        int id = generateId();

        //generate new primary key while it already exists
        while(this.artifacts.get(id) != null){
            id = generateId();
        }
        
        //create Artifact
        Artifact artifact = new Artifact(id,userId,title,description,latitude,longitude);
        
        //put artifact in map
        this.artifacts.put(id, artifact);
        
        return id;
    }

    /**
     * Retrieves an Artifact from database by its id
     * @param id
     * @return
     */
    @Override
    public Artifact getArtifact(int id) {
        return this.artifacts.get(id);
    }

    /**
     * Get all artifacts count
     * @return
     */
    @Override
    public int getAllArtifactsCount() {
        return this.artifacts.size();
    }

    /**
     * Get all artifacts
     * @return
     */
    @Override
    public List<Artifact> getAllArtifacts() {
        return new ArrayList<>(this.artifacts.values());
    }
    
}
