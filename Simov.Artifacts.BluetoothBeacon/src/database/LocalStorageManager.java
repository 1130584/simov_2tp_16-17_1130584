/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import model.Artifact;
import model.ArtifactsXMLRoot;

/**
 *
 * @author Admin
 */
public class LocalStorageManager {
    
    /**
     * XML file name
     */
    private String filename;
    
    /**
     * JAXBContext
     */
    private JAXBContext jaxbContext;
    
    /**
     * Constructor
     * @param filename XML file name
     * @throws JAXBException 
     */
    public LocalStorageManager(String filename) throws JAXBException{
        this.filename = filename;
        this.jaxbContext = JAXBContext.newInstance(ArtifactsXMLRoot.class);
    }
    
    public List<Artifact> load(){
        try{
            //create reader
            Unmarshaller unmarshaller = this.jaxbContext.createUnmarshaller();

            //read xml file
            File xmlFile = new File(this.filename);
            ArtifactsXMLRoot root = (ArtifactsXMLRoot) unmarshaller.unmarshal(xmlFile);

            System.out.println("== File '"+this.filename+"' loaded successfully");

            //get data
            return new ArrayList<>(root.getArtifacts());
        }catch(Exception ex){
            System.err.println("== Could not load File '"+this.filename+"'");
            return new ArrayList<Artifact>();
        }
    }
    
    /**
     * Save data to file
     * @param artifacts
     * @throws javax.xml.bind.JAXBException
     */
    public void save(List<Artifact> artifacts){
        try{
            //create root element with data
            ArtifactsXMLRoot root = new ArtifactsXMLRoot();
            root.setArtifacts(new ArrayList<>(artifacts));

            //create writter
            Marshaller marshaller = this.jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            //write xml file
            File xmlFile = new File(this.filename);
            marshaller.marshal(root,xmlFile);

            System.out.println("== File '"+this.filename+"' saved successfully");
        }catch(Exception ex){
            System.err.println("== Could not save data to File '"+this.filename+"'");
        }
    }
}
