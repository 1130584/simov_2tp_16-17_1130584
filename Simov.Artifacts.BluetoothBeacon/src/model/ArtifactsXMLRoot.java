/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Artifacts xml database file root element
 * @author Admin
 */
@XmlRootElement
public class ArtifactsXMLRoot {
    
    private List<Artifact> artifacts = new ArrayList<>();

    @XmlElementWrapper
    @XmlElement(name="artifact")
    public List<Artifact> getArtifacts() {
        return artifacts;
    }
  
    public void setArtifacts(List<Artifact> artifacts) {
        this.artifacts = artifacts;
    } 
}
