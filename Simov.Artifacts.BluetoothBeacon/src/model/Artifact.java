/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Represents an Artifact
 */
public class Artifact {

    public int id;
    public String userId;
    public String title;
    public String description;
    public String longitude;
    public String latitude;
    
    public Artifact(){
        this.id = -1;
        this.userId = "";
        this.title = "";
        this.description = "";
        this.longitude = "";
        this.latitude = "";
    }

    /**
     * Constructor
     * @param id
     * @param databaseId
     * @param userId
     * @param title
     * @param description
     * @param latitude
     * @param longitude
     */
    public Artifact(int id, String userId, String title, String description, String latitude, String longitude) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.description = description;
        this.longitude = longitude;
        this.latitude = latitude;
    }

}
