package simov.artifacts.database;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 15/12/2016.
 */

public class DatabaseTableDefinition {

    public static class TableField{
        public String name;
        public String type;

        public TableField(String name, String type){
            this.name = name;
            this.type = type;
        }
    }

    private String name;
    private List<TableField> fields;

    public DatabaseTableDefinition(String name){
        this.name = name;
        this.fields = new ArrayList<>();
    }

    public String getName(){
        return this.name;
    }

    public void addField(String name, String type){
        this.fields.add(new TableField(name,type));
    }

    public String getFieldsText(){
        String text = "";

        for(int i=0; i<this.fields.size(); i++){
            TableField field = this.fields.get(i);
            text += field.name + " " + field.type;

            if(i < this.fields.size()-1){
                text += ", ";
            }
        }

        return text;
    }
}
