package simov.artifacts.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * SQLite Database Helper
 */
public class SQLiteDatabaseHelper extends SQLiteOpenHelper {

    /**
     * Version and Name
     */
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "simov.artifacts.database";

    /**
     * Database content
     */
    private List<String> createStatements;
    private List<DatabaseTableDefinition> tables;

    /**
     * Context
     */
    private Context context;

    /**
     * Constructor
     * @param context
     * @param tables
     */
    public SQLiteDatabaseHelper(Context context, List<DatabaseTableDefinition> tables){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        this.context = context;
        this.tables = new ArrayList<>(tables);

        this.createStatements = new ArrayList<>();
        for(DatabaseTableDefinition table: tables){
            this.createStatements.add("CREATE TABLE IF NOT EXISTS " + table.getName() + " (" + table.getFieldsText() + ")");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase arg0) {
        for(String createStatement: this.createStatements) {
            arg0.execSQL(createStatement);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
        for(DatabaseTableDefinition table : this.tables){
            arg0.execSQL("DROP TABLE IF EXISTS " + table.getName());
        }
        onCreate(arg0);
    }
}
