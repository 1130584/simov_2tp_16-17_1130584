package simov.artifacts.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import simov.artifacts.model.Artifact;
import simov.artifacts.model.ArtifactDiscovered;
import simov.artifacts.utils.Utils;

/**
 * Database adapter
 */
public class ArtifactsLocalDatabase implements ArtifactsDatabase{

    /**
     * Device google account username
     */
    private String username;

    /**
     * SQLiteDatabaseHelper
     */
    private SQLiteDatabaseHelper dbHelper;

    /**
     * Database table names
     */
    private static final String ARTIFACT = "ARTIFACT";
    private static final String ARTIFACT_DISCOVERED = "ARTIFACT_DISCOVERED";

    /**
     * Database column names
     */
    private static final String ID = "ID";
    private static final String USER_ID = "USER_ID";
    private static final String SERVER_ID = "SERVER_ID";
    private static final String TITLE = "TITLE";
    private static final String DESCRIPTION = "DESCRIPTION";
    private static final String LONGITUDE = "LONGITUDE";
    private static final String LATITUDE = "LATITUDE";

    private static final String ARTIFACT_DATABASE_ID = "ARTIFACT_DATABASE_ID";
    private static final String DATE = "DATE";

    /**
     * Constructor
     * @param context
     */
    public ArtifactsLocalDatabase(Context context) {
        List<DatabaseTableDefinition> databaseTableDefinitions = new ArrayList<>();

        //create ARTIFACT table
        DatabaseTableDefinition artifactTable = new DatabaseTableDefinition(ARTIFACT);
        artifactTable.addField(ID,"INTEGER PRIMARY KEY AUTOINCREMENT");
        artifactTable.addField(SERVER_ID,"INTEGER");
        artifactTable.addField(USER_ID,"TEXT");
        artifactTable.addField(TITLE,"TEXT");
        artifactTable.addField(DESCRIPTION,"TEXT");
        artifactTable.addField(LATITUDE,"REAL");
        artifactTable.addField(LONGITUDE,"REAL");
        databaseTableDefinitions.add(artifactTable);

        //create ARTIFACT_DISCOVERED table
        DatabaseTableDefinition artifactDiscoveredTable = new DatabaseTableDefinition(ARTIFACT_DISCOVERED);
        artifactDiscoveredTable.addField(ID,"INTEGER PRIMARY KEY AUTOINCREMENT");
        artifactDiscoveredTable.addField(ARTIFACT_DATABASE_ID,"INTEGER");
        artifactDiscoveredTable.addField(DATE,"TEXT");
        databaseTableDefinitions.add(artifactDiscoveredTable);

        //create database helper
        this.dbHelper = new SQLiteDatabaseHelper(context, databaseTableDefinitions);
    }

    /**
     * Insert a new Artifact in database
     * @param title
     * @param description
     * @return
     */
    public boolean newArtifact(String userId, int serverId, String title, String description, double latitude, double longitude) {
        //open database
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {

            //insert
            ContentValues initialValues = new ContentValues();
            initialValues.put(USER_ID,userId);
            initialValues.put(SERVER_ID,serverId);
            initialValues.put(TITLE,title);
            initialValues.put(DESCRIPTION,description);
            initialValues.put(LATITUDE,latitude);
            initialValues.put(LONGITUDE,longitude);
            db.insert(ARTIFACT, null, initialValues);

            //close database
            db.close();
        } catch (SQLException sqlerror) {
            Log.v("Insert into table " + ARTIFACT + "error", sqlerror.getMessage());
            //close database
            db.close();
            return false;
        }
        return true;
    }

    /**
     * Update Artifact server id
     * @param artifactId
     * @param serverId
     * @return
     */
    public boolean updateArtifactServerId(int artifactId, int serverId){
        //open database
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {

            //update
            ContentValues initialValues = new ContentValues();
            initialValues.put(SERVER_ID,serverId);
            db.update(ARTIFACT, initialValues, ID+"="+artifactId, null);

            //close database
            db.close();
        } catch (SQLException sqlerror) {
            Log.v("Update table " + ARTIFACT + " row error", sqlerror.getMessage());
            //close database
            db.close();
            return false;
        }
        return true;
    }

    /**
     * Edit a Artifact of the database
     * @param title
     * @param description
     * @return
     */
    public boolean editArtifact(int id, String title, String description){
        //open database
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {

            //update
            ContentValues initialValues = new ContentValues();
            initialValues.put(TITLE,title);
            initialValues.put(DESCRIPTION,description);
            db.update(ARTIFACT, initialValues, ID+"="+id, null);

            //close database
            db.close();
            return true;
        } catch (SQLException sqlerror) {
            Log.v("Update table " + ARTIFACT + " row error", sqlerror.getMessage());
            //close database
            db.close();
            return false;
        }
    }

    /**
     * Delete an Artifact from the database
     * @param id
     * @return
     */
    public boolean deleteArtifact(int id){
        //open database
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {

            //delete
            int result = db.delete(ARTIFACT, ID+"="+id, null);

            //close database
            db.close();
            return result > 0;
        } catch (SQLException sqlerror) {
            Log.v("Delete row of table " + ARTIFACT + " error", sqlerror.getMessage());
            //close database
            db.close();
            return false;
        }
    }

    /**
     * Insert a new ArtifactDiscovered in database
     * @param databaseId
     * @return
     */
    public boolean newArtifactDiscovered(int databaseId){
        //open database
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {
            //get current date
            String date = Utils.getCurrentDate();

            //insert
            ContentValues initialValues = new ContentValues();
            initialValues.put(ARTIFACT_DATABASE_ID,databaseId);
            initialValues.put(DATE,date);
            db.insert(ARTIFACT_DISCOVERED, null, initialValues);

            //close database
            db.close();
        } catch (SQLException sqlerror) {
            Log.v("Insert into table " + ARTIFACT_DISCOVERED + "error", sqlerror.getMessage());
            //close database
            db.close();
            return false;
        }
        return true;
    }

    /**
     * If database already has a specific artifact
     * @param serverId
     * @return
     */
    public boolean hasArtifact(int serverId){
        ///open database
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //execute query
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + ARTIFACT + " WHERE " + SERVER_ID + "=" + serverId, null);

        //get count
        int count = 0;
        if(null != cursor){
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                count = cursor.getInt(0);
            }
            cursor.close();
        }

        db.close();
        return count == 1;
    }

    /**
     * Retrieves an Artifact from database by its id
     * @param id
     * @return
     */
    public Artifact getArtifact(int id) {
        //open database
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //execute query
        Cursor cursor = db.rawQuery("SELECT * FROM " + ARTIFACT + " WHERE " + ID + "=" + id, null);

        List<Artifact> artifacts = getArtifactsFromCursor(cursor);

        //get results from cursor
        Artifact artifact = artifacts != null && artifacts.size() >= 1 ? artifacts.get(0) : null;

        //close db
        db.close();
        return artifact;
    }

    /**
     * Retrieves an Artifact from database by its server id
     * @param serverId
     * @return
     */
    public Artifact getArtifactByServerId(int serverId){
        //open database
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //execute query
        Cursor cursor = db.rawQuery("SELECT * FROM " + ARTIFACT + " WHERE " + SERVER_ID + "=" + serverId, null);

        List<Artifact> artifacts = getArtifactsFromCursor(cursor);

        //get results from cursor
        Artifact artifact = artifacts != null && artifacts.size() >= 1 ? artifacts.get(0) : null;

        //close db
        db.close();
        return artifact;
    }

    /**
     * Get all artifacts of user
     * @return
     */
    public List<Artifact> getAllArtifactsOfUser(String username) {
        //open database
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //execute query
        Cursor cursor = db.rawQuery("SELECT * FROM " + ARTIFACT + " WHERE " + USER_ID + "= '" + username + "'", null);

        //get results from cursor
        List<Artifact> artifacts = getArtifactsFromCursor(cursor);

        //close database
        db.close();
        return artifacts;
    }

    /**
     * Get all artifacts of user count
     * @return
     */
    public int getAllArtifactsOfUserCount(String username) {
        //open database
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //execute query
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + ARTIFACT + " WHERE " + USER_ID + "= '" + username + "'", null);

        //get count
        int count = 0;
        if(null != cursor){
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                count = cursor.getInt(0);
            }
            cursor.close();
        }

        //close database
        db.close();
        return count;
    }

    /**
     * Get all artifacts count
     * @return
     */
    public int getAllArtifactsCount(){
        //open database
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //execute query
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + ARTIFACT, null);

        //get count
        int count = 0;
        if(null != cursor){
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                count = cursor.getInt(0);
            }
            cursor.close();
        }

        db.close();
        return count;
    }

    /**
     * Get all artifacts
     * @return
     */
    public List<Artifact> getAllArtifacts() {
        //open database
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //execute query
        Cursor cursor = db.rawQuery("SELECT * FROM " + ARTIFACT, null);

        //get results from cursor
        List<Artifact> artifacts = getArtifactsFromCursor(cursor);

        //close database
        db.close();
        return artifacts;
    }

    /**
     * Get all artifacts created count
     * @return
     */
    public int getAllArtifactsCreatedCount(String userId){
        //open database
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //execute query
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + ARTIFACT + " WHERE " + SERVER_ID + " = 0 AND " + USER_ID + " = '" + userId + "'", null);

        //get count
        int count = 0;
        if(null != cursor){
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                count = cursor.getInt(0);
            }
            cursor.close();
        }

        db.close();
        return count;
    }

    /**
     * Get all artifacts of user
     * @return
     */
    public List<Artifact> getAllArtifactsCreated(String userId){
        //open database
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //execute query
        Cursor cursor = db.rawQuery("SELECT * FROM " + ARTIFACT + " WHERE " + SERVER_ID + " = 0 AND " + USER_ID + " = '" + userId + "'", null);

        //get results from cursor
        List<Artifact> artifacts = getArtifactsFromCursor(cursor);

        //close database
        db.close();
        return artifacts;
    }

    /**
     * Get all artifacts discovered count
     * @return
     */
    public int getAllArtifactsDiscoveredCount(){
        //open database
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //execute query
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + ARTIFACT_DISCOVERED, null);

        //get count
        int count = 0;
        if(null != cursor){
            if(cursor.getCount() > 0){
                cursor.moveToFirst();
                count = cursor.getInt(0);
            }
            cursor.close();
        }

        db.close();
        return count;
    }

    /**
     * Get all artifacts discovered
     * @return
     */
    public List<ArtifactDiscovered> getAllArtifactsDiscovered() {
        //open database
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //execute query
        Cursor cursor = db.rawQuery("SELECT * FROM " + ARTIFACT_DISCOVERED, null);

        //get results from cursor
        List<ArtifactDiscovered> artifactsDiscovered = getArtifactsDiscoveredFromCursor(cursor);

        //close database
        db.close();
        return artifactsDiscovered;
    }

    /**
     * Get a list of Artifacts from a Query cursor
     * @param cursor
     * @return
     */
    private List<Artifact> getArtifactsFromCursor(Cursor cursor){
        List<Artifact> artifacts = new ArrayList<>();

        //get results from cursor
        if(cursor != null) {
            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {

                Artifact artifact = new Artifact(cursor.getInt(0),
                        cursor.getInt(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getDouble(5),
                        cursor.getDouble(6));

                artifacts.add(artifact);

                if(cursor.getCount() > 1) {
                    cursor.moveToNext();
                }
            }

            cursor.close();
        }

        return artifacts;
    }

    /**
     * Get a list of Artifacts Discovered from a Query cursor
     * @param cursor
     * @return
     */
    private List<ArtifactDiscovered> getArtifactsDiscoveredFromCursor(Cursor cursor){
        List<ArtifactDiscovered> artifactsDiscovered = new ArrayList<>();

        //get results from cursor
        if(cursor != null) {
            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {

                ArtifactDiscovered artifactDiscovered = new ArtifactDiscovered(cursor.getInt(0),
                        cursor.getInt(1),
                        cursor.getString(2));

                artifactsDiscovered.add(artifactDiscovered);

                cursor.moveToNext();
            }

            cursor.close();
        }

        return artifactsDiscovered;
    }

}
