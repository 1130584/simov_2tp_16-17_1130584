package simov.artifacts.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import simov.artifacts.model.Artifact;
import simov.artifacts.model.ArtifactDiscovered;

/**
 * Created by Admin on 17/12/2016.
 */

public interface ArtifactsDatabase {

    /**
     * Insert a new Artifact in database
     * @param title
     * @param description
     * @return
     */
    public boolean newArtifact(String userId, int serverId, String title, String description, double latitude, double longitude);

    /**
     * Update Artifact server id
     * @param artifactId
     * @param serverId
     * @return
     */
    public boolean updateArtifactServerId(int artifactId, int serverId);

    /**
     * Edit a Artifact of the database
     * @param title
     * @param description
     * @return
     */
    public boolean editArtifact(int id, String title, String description);

    /**
     * Delete an Artifact from the database
     * @param id
     * @return
     */
    public boolean deleteArtifact(int id);

    /**
     * If database already has a specific artifact
     * @param serverId
     * @return
     */
    public boolean hasArtifact(int serverId);

    /**
     * Insert a new ArtifactDiscovered in database
     * @param databaseId
     * @return
     */
    public boolean newArtifactDiscovered(int databaseId);

    /**
     * Retrieves an Artifact from database by its id
     * @param id
     * @return
     */
    public Artifact getArtifact(int id);

    /**
     * Retrieves an Artifact from database by its server id
     * @param serverId
     * @return
     */
    public Artifact getArtifactByServerId(int serverId);

    /**
     * Get all artifacts count
     * @return
     */
    public int getAllArtifactsCount();

    /**
     * Get all artifacts
     * @return
     */
    public List<Artifact> getAllArtifacts();

    /**
     * Get all artifacts of user
     * @return
     */
    public List<Artifact> getAllArtifactsOfUser(String username);

    /**
     * Get all artifacts of user count
     * @return
     */
    public int getAllArtifactsOfUserCount(String username);

    /**
     * Get all artifacts created count
     * @return
     */
    public int getAllArtifactsCreatedCount(String userId);

    /**
     * Get all artifacts of user created
     * @return
     */
    public List<Artifact> getAllArtifactsCreated(String userId);

    /**
     * Get all artifacts created count
     * @return
     */
    public int getAllArtifactsDiscoveredCount();

    /**
     * Get all artifacts discovered
     * @return
     */
    public List<ArtifactDiscovered> getAllArtifactsDiscovered();
}
