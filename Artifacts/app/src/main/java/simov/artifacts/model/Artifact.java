package simov.artifacts.model;

/**
 * Represents an Artifact
 */
public class Artifact {

    public int id;
    public int serverId;
    public String userId;
    public String title;
    public String description;
    public double longitude;
    public double latitude;

    /**
     * Constructor
     * @param id
     * @param serverId
     * @param userId
     * @param title
     * @param description
     * @param latitude
     * @param longitude
     */
    public Artifact(int id, int serverId, String userId, String title, String description, double latitude, double longitude) {
        this.id = id;
        this.serverId = serverId;
        this.userId = userId;
        this.title = title;
        this.description = description;
        this.longitude = longitude;
        this.latitude = latitude;
    }

}
