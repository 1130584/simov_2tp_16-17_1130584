package simov.artifacts.model;

/**
 * Represents an definition of a discovered Artifact showing the date when it was discovered
 */
public class ArtifactDiscovered {

    public int id;
    public int artifactId;
    public String date;

    /**
     * COnstructor
     * @param id
     * @param artifactId
     * @param date
     */
    public ArtifactDiscovered(int id, int artifactId, String date){
        this.id = id;
        this.artifactId = artifactId;
        this.date = date;
    }
}
