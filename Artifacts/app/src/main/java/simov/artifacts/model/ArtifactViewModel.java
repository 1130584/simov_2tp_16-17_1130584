package simov.artifacts.model;

/**
 * Used to bring Artifacts to the view to be displayed
 */
public class ArtifactViewModel {

    public static enum ArtifactKind {
        CREATED,
        DISCOVERED,
        UNDISCOVERED
    }

    public static final String HIDDEN_FIELD = "{{#HIDDEN_FIELD#}}";

    public Artifact artifact;
    public ArtifactKind kind;
    public String dateDiscovered;

    /**
     * Constructor
     * @param dateDiscovered
     * @param kind
     * @param artifact
     */
    public ArtifactViewModel(String dateDiscovered, ArtifactKind kind, Artifact artifact) {
        this.dateDiscovered = dateDiscovered;
        this.kind = kind;
        this.artifact = kind == ArtifactKind.DISCOVERED || kind == ArtifactKind.CREATED ? artifact : getReducedVersion(artifact);
    }

    /**
     * Get reduce version of this Artifact to show to the user as an undiscovered Artifact
     * @param artifact
     * @return
     */
    private Artifact getReducedVersion(Artifact artifact){
        return new Artifact(artifact.id,artifact.serverId,artifact.userId,HIDDEN_FIELD,HIDDEN_FIELD,artifact.latitude,artifact.longitude);
    }
}
