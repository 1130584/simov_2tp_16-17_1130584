package simov.artifacts.ui.ListPage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import simov.artifacts.R;
import simov.artifacts.business.ArtifactsRepository;
import simov.artifacts.database.ArtifactsDatabase;
import simov.artifacts.model.ArtifactViewModel;
import simov.artifacts.ui.MainPage.Sections.StatsSection.StatsSectionFragment;
import simov.artifacts.ui.ListPage.ArtifactsListCustomListAdapter.ArtifactListItem;
import simov.artifacts.ui.ViewArtifactActivity;
import simov.artifacts.utils.Utils;

public class ArtifactsListActivity extends AppCompatActivity {

    public static final String ARTIFACTS_TO_DISPLAY = Utils.getAppId()+"ARTIFACTS_TO_DISPLAY";
    public static final String TITLE_TO_DISPLAY = Utils.getAppId()+"TITLE_TO_DISPLAY";

    /**
     * Artifacts repository
     */
    private ArtifactsRepository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artifacts_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String title = getIntent().getExtras().getString(TITLE_TO_DISPLAY);
        int artifactsToDisplay = getIntent().getExtras().getInt(ARTIFACTS_TO_DISPLAY);

        //load repository
        ArtifactsDatabase database = Utils.getAppDatabase(this);
        this.repository = new ArtifactsRepository(this,database);

        //set title
        setTitle(title);

        //setup artifacts list
        ListView artifactsListView = (ListView) findViewById(R.id.artifacts_list);

        //get Artifacts list
        final ArrayList<ArtifactListItem> artifactsListData = getArtifactsListItems(artifactsToDisplay);

        //create adapter
        final ArtifactsListCustomListAdapter adapter = new ArtifactsListCustomListAdapter(getBaseContext(), artifactsListData);

        // Assign adapter to ListView
        artifactsListView.setAdapter(adapter);

        // ListView Item Click Listener
        artifactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //Goto an Activity that shows the preview of the Artifact
                ArtifactListItem item = (ArtifactListItem)adapter.getItem(position);

                if(item.artifact.kind == ArtifactViewModel.ArtifactKind.CREATED || item.artifact.kind == ArtifactViewModel.ArtifactKind.DISCOVERED) {
                    Intent intent = new Intent(getBaseContext(), ViewArtifactActivity.class);
                    intent.putExtra(ViewArtifactActivity.ARTIFACT_ID, item.artifact.artifact.id);
                    startActivity(intent);
                }
            }

        });
    }

    private ArrayList<ArtifactListItem> getArtifactsListItems(int artifactsToDisplay){
        ArrayList<ArtifactListItem> artifactsListData = new ArrayList<>();

        //get all Artifacts
        List<ArtifactViewModel> artifacts = null;

        switch(artifactsToDisplay){
            case StatsSectionFragment.ARTIFACTS_ACQUIRED:
                artifacts = this.repository.getAllArtifacts(true,false,false);
                break;
            case StatsSectionFragment.ARTIFACTS_TO_ACQUIRE:
                artifacts = this.repository.getAllArtifacts(false,true,false);
                break;
            case StatsSectionFragment.ARTIFACTS_CREATED:
                artifacts = this.repository.getAllArtifacts(false,false,true);
                break;
            case StatsSectionFragment.ARTIFACTS_TOTAL:
                artifacts = this.repository.getAllArtifacts(true,true,true);
                break;
        }

        if(artifacts != null){
            for(ArtifactViewModel artifact : artifacts){
                ArtifactListItem listItem = null;

                switch(artifact.kind){
                    case CREATED:
                        listItem = new ArtifactListItem(artifact.artifact.title,artifact.artifact.userId != null ? artifact.artifact.userId : "Unknown User",artifact);
                        break;
                    case DISCOVERED:
                        listItem = new ArtifactListItem(artifact.artifact.title,(artifact.artifact.userId != null ? artifact.artifact.userId : "Unknown User") + " - " + artifact.dateDiscovered,artifact);
                        break;
                    case UNDISCOVERED:
                        listItem = new ArtifactListItem("Artifact",artifact.artifact.userId != null ? artifact.artifact.userId : "Unknown User",artifact);
                        break;
                }

                if(listItem != null){
                    artifactsListData.add(listItem);
                }
            }
        }

        return artifactsListData;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
