package simov.artifacts.ui.MainPage.Sections;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import simov.artifacts.R;
import simov.artifacts.model.Artifact;
import simov.artifacts.model.ArtifactViewModel;
import simov.artifacts.ui.AddEditArtifactActivity;
import simov.artifacts.ui.MainPage.MainPageActivity;
import simov.artifacts.ui.SettingsActivity;
import simov.artifacts.ui.ViewArtifactActivity;


/**
 * Main page fragment for the Map section
 */
public class MapSectionFragment extends SectionFragment implements OnMapReadyCallback, LocationListener {

    /**
    * Request codes
    */
    private static final int ADD_EDIT_ARTIFACT_REQUEST_CODE = 1500;
    private static final int VIEW_ARTIFACT_REQUEST_CODE = 2500;

    /**
     * Access location permissions
     */
    private static final String[] LOCATION_PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    /**
     * Map
     */
    private GoogleMap map;

    /**
     * Last known device location
     */
    private Location deviceLocation;

    /**
     * Device location marker
     */
    private Marker deviceLocationMarker;

    /**
     * Artifacts markers
     */
    private List<Marker> artifactsMarkers = new ArrayList<>();

    /**
     * If app has access to current device location
     */
    private boolean hasAccessTolocation;

    /**
     * If map starts with a n initial location
     */
    private boolean initialLocation = false;

    /**
     * If GPS location is generated artificially
     */
    private boolean artificalLocation = false;

    /**
     * Constructor
     */
    public MapSectionFragment() {
        super("Map");
    }

    /**
     * Set initial map location
     */
    public void setInitialMapLocation(double latitude, double longitude){
        this.deviceLocation = new Location("");
        this.deviceLocation.setLatitude(latitude);
        this.deviceLocation.setLongitude(longitude);
        this.initialLocation = true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        //setup ADD float action button
        rootView.findViewById(R.id.addButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCurrentLocation();
                if(deviceLocation != null){
                    Intent intent = new Intent(getContext(), AddEditArtifactActivity.class);
                    intent.putExtra(AddEditArtifactActivity.MODE,AddEditArtifactActivity.NEW_ARTIFACT_MODE);
                    intent.putExtra(AddEditArtifactActivity.ARTIFACT_LAT,deviceLocation.getLatitude());
                    intent.putExtra(AddEditArtifactActivity.ARTIFACT_LON,deviceLocation.getLongitude());
                    startActivityForResult(intent,ADD_EDIT_ARTIFACT_REQUEST_CODE);
                }
            }
        });

        //setup GET_LOCATION float action button
        rootView.findViewById(R.id.getPositionButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCurrentLocation();
            }
        });

        //setup google map
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //request location permissions
        this.requestLocationPermissions();

        //get preferences
        this.getPreferences();

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_EDIT_ARTIFACT_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                this.setupMapMarkers();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        //disable toolbar
        map.getUiSettings().setMapToolbarEnabled(false);

        //set marker click listener
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            public void onInfoWindowClick(Marker marker)
            {
                Intent intent = new Intent(getContext(), ViewArtifactActivity.class);

                if (marker.getTag() instanceof ArtifactViewModel) {
                    ArtifactViewModel artifactViewModel = (ArtifactViewModel) marker.getTag();

                    if(artifactViewModel.kind == ArtifactViewModel.ArtifactKind.DISCOVERED
                            || artifactViewModel.kind == ArtifactViewModel.ArtifactKind.CREATED) {
                        intent.putExtra(ViewArtifactActivity.ARTIFACT_ID, artifactViewModel.artifact.id);
                        startActivity(intent);
                    }
                }
            }
        });

        //add Artifact markers
        if(this.repository != null) {
            this.setupMapMarkers();
        }

        //create location marker
        this.deviceLocationMarker = map.addMarker(new MarkerOptions().position(new LatLng(0,0)).title("Me").visible(false));

        //move camera to initial location if it has defined
        if(this.initialLocation == true) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(this.deviceLocation.getLatitude(),this.deviceLocation.getLongitude()), 10));
        }else{
            //go to current position
            this.getCurrentLocation();
        }
    }

    @Override
    public void onPauseFragment() {

    }

    @Override
    public void onResumeFragment() {
        //get shared preference
        this.getPreferences();
    }

    @Override
    public void onResume() {
        super.onResume();
        this.setupMapMarkers();

        //get shared preference
        this.getPreferences();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        this.hasAccessTolocation = false;

        if(grantResults.length > 0){
            for(int i=0; i<permissions.length; i++){
                if(grantResults[i] == PackageManager.PERMISSION_GRANTED){
                    this.hasAccessTolocation = true;
                    this.requestLocationUpdates();
                }
            }
        }
    }

    /**
     * Get preferences
     */
    private void getPreferences(){
        //get shared preference
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences(SettingsActivity.PREFERENCES_NAME, Context.MODE_PRIVATE);
        this.artificalLocation = sharedpreferences.getBoolean(getString(R.string.sh_pref_allow_artifical_location),false);
    }

    /**
     * Request location updates
     */
    private void requestLocationUpdates(){
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 10, (LocationListener) this);
        }catch(SecurityException ex){
            Toast.makeText(this.getContext(),"Location services not available",Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Get current device location
     */
    private void getCurrentLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        try {

            if(this.artificalLocation == true) {

                //set random current location
                this.deviceLocation = new Location("");
                this.deviceLocation.setLatitude(41.234647 + Math.random() * 0.001);
                this.deviceLocation.setLongitude( -8.624035 + Math.random() * 0.001);

                List<ArtifactViewModel> artifacts = this.repository.getAllArtifacts(false, true, false);
                if (artifacts != null && artifacts.size() > 0) {

                    // generate random index with a probability of 25% of the generated location
                    // corresponds to a location of an undiscovered Artifact
                    // this is just to simulate Artifact discovery
                    int indexRandom = (int) (Math.random() * artifacts.size() * 4);

                    for (int i = 0; i < artifacts.size(); i++) {
                        ArtifactViewModel artifact = artifacts.get(i);
                        if (i == indexRandom) {
                            this.deviceLocation.setLatitude(artifact.artifact.latitude);
                            this.deviceLocation.setLongitude(artifact.artifact.longitude);

                            //register artifact discovered
                            Artifact artifactDiscovered = repository.newArtifactDiscovered(artifact.artifact.serverId);

                            //get notification manager
                            NotificationManager notificationManager = (NotificationManager) this.getActivity().getSystemService(Context.NOTIFICATION_SERVICE);

                            //create intent that calls the main activity
                            Intent intent = new Intent(this.getActivity(), MainPageActivity.class);
                            PendingIntent pIntent = PendingIntent.getActivity(this.getActivity(), (int) System.currentTimeMillis(), intent, 0);

                            Artifact atf = this.repository.getArtifactByServerId(artifact.artifact.serverId);
                            // build notification
                            Notification notification = new Notification.Builder(this.getActivity())
                                    .setContentTitle("New Artifact discovered!")
                                    .setContentText(atf.title)
                                    .setSmallIcon(R.drawable.ic_location)
                                    .setContentIntent(pIntent)
                                    .setAutoCancel(true)
                                    .build();

                            //fire notification
                            notificationManager.notify(0, notification);

                            //setup markers in the map
                            this.setupMapMarkers();

                            //show message
                            Toast.makeText(getActivity(),"New Artifact discovered!",Toast.LENGTH_LONG).show();
                            break;
                        }
                    }
                }
            }else{
                //get last known location
                this.deviceLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                //check if current location was retrieved
                if (this.deviceLocation == null) {
                    throw new SecurityException();
                }
            }

            //set current location marker position
            LatLng position = this.updateCurrentLocationMarker();

            //move camera
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 10));

        }catch (SecurityException ex){
            Toast.makeText(this.getContext(),"Current location not available",Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Updates the current location marker
     */
    private LatLng updateCurrentLocationMarker(){
        LatLng position = new LatLng(this.deviceLocation.getLatitude(), this.deviceLocation.getLongitude());
        this.deviceLocationMarker.setVisible(true);
        this.deviceLocationMarker.setPosition(position);
        return position;
    }

    /**
     * Request location permissions
     */
    private void requestLocationPermissions(){
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), LOCATION_PERMISSIONS, 0);
        }else{
            this.hasAccessTolocation = true;
            this.requestLocationUpdates();
        }
    }

    /**
      * Operation to place Artifact markers in the map
      */
    private void setupMapMarkers(){
        if(this.map == null){
            return;
        }

        //clear old markers
        for(Marker marker: this.artifactsMarkers){
            marker.remove();
        }

        //add new markers
        this.artifactsMarkers = new ArrayList<>();

        //get all artifacts
        List<ArtifactViewModel> artifactViewModels = repository.getAllArtifacts(true, true, true);

        for (ArtifactViewModel artifactViewModel : artifactViewModels) {

            //create location
            LatLng location = new LatLng(artifactViewModel.artifact.latitude, artifactViewModel.artifact.longitude);

            //create marker options
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(location);

            switch (artifactViewModel.kind) {
                case CREATED:
                    markerOptions = markerOptions
                            .title(artifactViewModel.artifact.title)
                            .snippet(artifactViewModel.artifact.userId)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.orange_mark));
                    break;
                case DISCOVERED:
                    markerOptions = markerOptions
                            .title(artifactViewModel.artifact.title)
                            .snippet(artifactViewModel.artifact.userId)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.green_mark));
                    break;
                case UNDISCOVERED:
                    markerOptions = markerOptions
                            .title(artifactViewModel.artifact.userId)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.blue_mark));
                    break;
            }

            //add marker
            Marker marker = map.addMarker(markerOptions);

            //store ArtifactViewModel in mark tag
            marker.setTag(artifactViewModel);

            this.artifactsMarkers.add(marker);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        this.deviceLocation = location;
        this.updateCurrentLocationMarker();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}