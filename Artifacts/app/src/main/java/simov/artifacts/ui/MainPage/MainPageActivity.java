package simov.artifacts.ui.MainPage;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import simov.artifacts.R;
import simov.artifacts.business.ArtifactsRepository;
import simov.artifacts.database.ArtifactsDatabase;
import simov.artifacts.database.ArtifactsLocalDatabase;
import simov.artifacts.ui.MainPage.Sections.MapSectionFragment;
import simov.artifacts.ui.MainPage.Sections.SectionFragment;
import simov.artifacts.ui.MainPage.Sections.StatsSection.StatsSectionFragment;
import simov.artifacts.ui.SettingsActivity;
import simov.artifacts.ui.SynchronizationPage.SyncActivity;
import simov.artifacts.utils.Utils;

public class MainPageActivity extends AppCompatActivity {

    public static final String INITIAL_LOCATION = Utils.getAppId() + ".MainPageActivity.INITIAL_LOCATION";
    public static final String ARTIFACT_FOUND_LATITUDE = Utils.getAppId() + ".MainPageActivity.ARTIFACT_FOUND_LATITUDE";
    public static final String ARTIFACT_FOUND_LONGITUDE = Utils.getAppId() + ".MainPageActivity.ARTIFACT_FOUND_LONGITUDE";

    /**
     * Artifacts Repository
     */
    private ArtifactsRepository repository;


    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private MainPageSectionsPagerAdapter sectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);

        //get initial map location
        boolean initialLocation = false;
        double latitude = 0;
        double longitude = 0;

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if(extras != null) {
            initialLocation = getIntent().getExtras().getBoolean(INITIAL_LOCATION);
            latitude = getIntent().getExtras().getDouble(ARTIFACT_FOUND_LATITUDE);
            longitude = getIntent().getExtras().getDouble(ARTIFACT_FOUND_LONGITUDE);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.GET_ACCOUNTS}, 0);
        }else{
            //get the android device username
            String username = this.getUsername(this);
            if(username != null) {
                Utils.setUsername(username);
            }else{
                //show error and close app
                Toast.makeText(this,"Cannot get username",Toast.LENGTH_LONG).show();
                finish();
            }
        }

        //create database
        ArtifactsDatabase database = Utils.getAppDatabase(this);

        //create repository
        this.repository = new ArtifactsRepository(this,database);

        //setup GUI
        this.setupGUI(initialLocation,latitude,longitude);
    }

    /**
     * Setup GUI elements
     */
    private void setupGUI(boolean initialLocation, double latitude, double longitude){
        //setup the Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //setup page sections
        List<SectionFragment> fragments = new ArrayList<>();
        //map
        MapSectionFragment map = new MapSectionFragment();
        map.setRepository(this.repository);
        if(initialLocation == true) {
            map.setInitialMapLocation(latitude, longitude);
        }
        fragments.add(map);
        //stats
        StatsSectionFragment stats = new StatsSectionFragment();
        stats.setRepository(this.repository);
        fragments.add(stats);

        //setup the ViewPager with the sections adapter
        this.sectionsPagerAdapter = new MainPageSectionsPagerAdapter(getSupportFragmentManager(), fragments);
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(sectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(pageChangeListener);

        //setup the TabLayout
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sync:
                Intent intentSync = new Intent(this, SyncActivity.class);
                startActivity(intentSync);
                return true;
            case R.id.action_settings:
                Intent intentSettings = new Intent(this, SettingsActivity.class);
                startActivity(intentSettings);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        if(grantResults.length > 0){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                String username = this.getUsername(this);
                if (username != null) {
                    Utils.setUsername(username);
                }
            }else{
                //show error and close app
            }
        }
    }

    /**
     * On page switch listener
     */
    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {

        int currentPosition = 0;

        @Override
        public void onPageSelected(int newPosition) {

            SectionFragment fragmentToShow = (SectionFragment)sectionsPagerAdapter.getItem(newPosition);
            fragmentToShow.onResumeFragment();

            SectionFragment fragmentToHide = (SectionFragment)sectionsPagerAdapter.getItem(currentPosition);
            fragmentToHide.onPauseFragment();

            currentPosition = newPosition;
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) { }

        public void onPageScrollStateChanged(int arg0) { }
    };

    /**
     * Returns the device google account username (first part of the email)
     *
     * @return
     */
    @Nullable
    private String getUsername(Context context) {
        try {
            AccountManager manager = AccountManager.get(context);

            Account[] accounts = manager.getAccountsByType("com.google");
            List<String> possibleEmails = new LinkedList<String>();

            for (Account account : accounts) {
                possibleEmails.add(account.name);
            }

            if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
                String email = possibleEmails.get(0);
                String[] parts = email.split("@");

                if (parts.length > 1)
                    return parts[0] == null ? "unknown" : parts[0];
            }

            return "unknown";
        }catch (SecurityException ex){
            return null;
        }
    }
}
