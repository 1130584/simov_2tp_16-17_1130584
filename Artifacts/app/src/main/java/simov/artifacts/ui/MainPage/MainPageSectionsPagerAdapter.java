package simov.artifacts.ui.MainPage;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import simov.artifacts.ui.MainPage.Sections.MapSectionFragment;
import simov.artifacts.ui.MainPage.Sections.SectionFragment;
import simov.artifacts.ui.MainPage.Sections.StatsSection.StatsSectionFragment;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages in the main activity of the application.
 */
public class MainPageSectionsPagerAdapter extends FragmentPagerAdapter {

    /**
     * Fragments
     */
    private List<SectionFragment> fragments;

    /**
     * Constructor
     * @param fragmentManager the FragmentManager
     */
    public MainPageSectionsPagerAdapter(FragmentManager fragmentManager,List<SectionFragment> fragments) {
        super(fragmentManager);
        this.fragments = new ArrayList<SectionFragment>(fragments);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        SectionFragment fragment = fragments.get(position);
        if(fragment != null){
            return fragment.getTitle();
        }
        return null;
    }
}
