package simov.artifacts.ui.SynchronizationPage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import simov.artifacts.R;
import simov.artifacts.model.ArtifactViewModel;

/**
 * DevicesListCustomListAdapter
 */
public class DevicesListCustomListAdapter extends BaseAdapter {

    /**
     * Devices list data
     */
    private ArrayList<DeviceListItem> listData;

    /**
     * Layout inflater
     */
    private LayoutInflater layoutInflater;

    /**
     * Constructor
     * @param context
     */
    public DevicesListCustomListAdapter(Context context) {
        this.listData = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
    }

    /**
     * Add new device to list
     * @param name
     */
    public void addDevice(String name){
        this.listData.add(new DeviceListItem(name));
        this.notifyDataSetChanged();
    }

    /**
     * Clear devices from list
     */
    public void clearDevices(){
        this.listData.clear();
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * List item layout
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = layoutInflater.inflate(R.layout.devices_list_row, null);

        ImageView image = (ImageView) rowView.findViewById(R.id.device_icon);
        TextView name = (TextView) rowView.findViewById(R.id.device_name);

        if(listData.size() > 0 && listData != null){
            DeviceListItem item = listData.get(position);
            if(item != null){
                image.setImageResource(R.drawable.ic_device);
                name.setText(item.name);
            }
        }

        return rowView;
    }

    /**
     * List item
     */
    public static class DeviceListItem{

        public String name;

        /**
         * Constructor
         */
        public DeviceListItem(String name){
            this.name = name;
        }
    }
}
