package simov.artifacts.ui.SynchronizationPage;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

import simov.artifacts.R;
import simov.artifacts.business.ArtifactsRepository;
import simov.artifacts.business.ArtifactsSynchronization;
import simov.artifacts.business.ArtifactsSynchronizationHostThreadInterface;
import simov.artifacts.communication.BluetoothCommunicationManager;
import simov.artifacts.communication.BluetoothDeviceListActivity;
import simov.artifacts.database.ArtifactsDatabase;
import simov.artifacts.ui.ListPage.ArtifactsListCustomListAdapter;
import simov.artifacts.ui.ViewArtifactActivity;
import simov.artifacts.utils.Utils;

public class SyncActivity extends BluetoothDeviceListActivity implements ArtifactsSynchronizationHostThreadInterface{

    private static final int OPERATION_NONE = 0;
    private static final int OPERATION_DISCOVERY = 1;
    private static final int OPERATION_SYNC = 2;


    /**
     * Bluetooth communication manager
     */
    private BluetoothCommunicationManager bluetoothCommunicationManager;

    /**
     * Devices list adapter
     */
    private DevicesListCustomListAdapter adapter;

    /**
     * Connected device name
     */
    private String conectedDeviceName;

    /**
     * Progress dialog
     */
    private AlertDialog dialog;

    /**
     * Synchronization task
     */
    private ArtifactsSynchronization synchronizationTask;

    /**
     * IThe operation that is running
     */
    private int runningOperation = OPERATION_NONE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Synchronization");

        final Activity self = this;

        //load repository
        ArtifactsDatabase database = Utils.getAppDatabase(this);
        final ArtifactsRepository repository = new ArtifactsRepository(this,database);

        //get default bluetooth adapter
        try {
            this.bluetoothCommunicationManager = new BluetoothCommunicationManager(this);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
            finish();
        }

        //turn on bluetooth
        this.bluetoothCommunicationManager.turnBluetoothOn(this);

        //Create progress dialog
        this.dialog = new AlertDialog.Builder(this)
                .setTitle("Synchronization")
                .setMessage("...")
                .setIcon(R.drawable.ic_sync_dark)
                .setCancelable(false)
                .create();

        //set cancel action on dialog button
        this.dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(runningOperation == OPERATION_DISCOVERY){
                    bluetoothCommunicationManager.stopDiscoverDevices();
                }else if(runningOperation == OPERATION_SYNC){
                    synchronizationTask.cancel(true);
                }
            }
        });

        //setup found devices list
        ListView devicesList = (ListView) findViewById(R.id.devices_list);

        //create adapter
        this.adapter = new DevicesListCustomListAdapter(getBaseContext());

        // Assign adapter to ListView
        devicesList.setAdapter(adapter);

        //start discover devices
        this.discoverDevices();

        // ListView Item Click Listener
        devicesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if(runningOperation == OPERATION_SYNC){
                    return;
                }
                //Goto an Activity that shows the preview of the Artifact
                DevicesListCustomListAdapter.DeviceListItem item = (DevicesListCustomListAdapter.DeviceListItem)adapter.getItem(position);

                //attempt to connect to selected device
                BluetoothSocket beaconSocket = bluetoothCommunicationManager.connectWithBeaconDevice(item.name);

                if(beaconSocket != null){
                    conectedDeviceName = item.name;

                    //start synchronization
                    synchronizationTask = new ArtifactsSynchronization(self, (ArtifactsSynchronizationHostThreadInterface) self,beaconSocket,repository);
                    synchronizationTask.execute();
                }else{
                    Toast.makeText(getApplicationContext(),"Device is not a valid Artifacts Beacon",Toast.LENGTH_LONG).show();
                    conectedDeviceName = null;
                }
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sync_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                this.discoverDevices();
                break;
            case android.R.id.home:
                this.bluetoothCommunicationManager.stopDiscoverDevices();
                setResult(RESULT_CANCELED);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void bluetoothStateChanged(int state){
        switch (state){
            case BluetoothAdapter.STATE_ON:
                this.discoverDevices();
                break;
            case BluetoothAdapter.STATE_OFF:
                Toast.makeText(this,"Bluetooth turned off",Toast.LENGTH_LONG).show();
                break;
            case BluetoothAdapter.STATE_CONNECTING:
                this.dialog.setMessage("Connecting to device...");
                this.dialog.show();
                break;
            case BluetoothAdapter.STATE_CONNECTED:
                Toast.makeText(this,"Connected to device: "+this.conectedDeviceName,Toast.LENGTH_LONG).show();
                break;
            case BluetoothAdapter.STATE_DISCONNECTED:
                Toast.makeText(this,"Disconnected from device",Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void bluetoothDiscoveryStarted(){
        this.runningOperation = OPERATION_DISCOVERY;
        this.showDialog("Searching for devices...");
    }

    @Override
    public void bluetoothDiscoveryFinished(){
        this.runningOperation = OPERATION_NONE;
        this.hideDialog();

        this.hideNoDevicesFoundText();
    }

    @Override
    public void newDeviceFound(String name) {
        this.adapter.addDevice(name);
        this.hideNoDevicesFoundText();
    }

    @Override
    public void onSynchronizationStart() {
        this.runningOperation = OPERATION_SYNC;
        this.showDialog("...");
    }

    @Override
    public void onSynchronizationProgressUpdate(String progress) {
        this.dialog.setMessage(progress);
    }

    @Override
    public void onSynchronizationFinish(ArtifactsSynchronization.Result result) {
        this.runningOperation = OPERATION_NONE;

        String message = "";
        if(result != null) {
            if (result.errors.size() > 0) {
                for (String error : result.errors) {
                    message += error + "\n";
                }
            } else {
                message = "Synchronization results\n\n" +
                        "Artifacts uploaded: " + result.artifactsUploaded + "\n\n" +
                        "Artifacts downloaded: " + result.artifactsDownloaded;
            }
        }else{
            message = "Canceled";
        }

        this.dialog.setMessage(message);
        this.dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setText("Ok");
    }

    private void hideNoDevicesFoundText(){
        if(this.adapter.getCount() > 0) {
            TextView noDevicesMessage = (TextView) findViewById(R.id.no_devices_message);
            noDevicesMessage.setVisibility(View.GONE);
        }
    }

    /**
     * Discovers bluetooth devices
     */
    private void discoverDevices(){
        //clear devices
        this.adapter.clearDevices();
        //show message
        TextView noDevicesMessage = (TextView) findViewById(R.id.no_devices_message);
        noDevicesMessage.setVisibility(View.VISIBLE);
        //start discovery
        this.bluetoothCommunicationManager.startDiscoverDevices(this);
    }

    /**
     * Show dialog
     * @param initialMessage
     */
    private void showDialog(String initialMessage){
        this.dialog.setMessage(initialMessage);
        this.dialog.show();
        this.dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setText("Cancel");
    }

    /**
     * Hide dialog
     */
    private void hideDialog(){
        this.dialog.setMessage("...");
        this.dialog.hide();
    }
}
