package simov.artifacts.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import simov.artifacts.R;
import simov.artifacts.business.ArtifactsRepository;
import simov.artifacts.communication.CommunicationProtocol;
import simov.artifacts.database.ArtifactsDatabase;
import simov.artifacts.utils.Utils;

/**
 * Activity to edit the properties of an Artifact already existant or of a new one
 */
public class AddEditArtifactActivity extends AppCompatActivity {

    //Constants for intent parameters
    public static String EDIT_ARTIFACT_MODE = "EDIT";
    public static String NEW_ARTIFACT_MODE = "NEW";
    public static String MODE = Utils.getAppId()+".ui.AddEditArtifactActivity.mode";
    public static String ARTIFACT_ID = Utils.getAppId()+".ui.AddEditArtifactActivity.Artifact.id";
    public static String ARTIFACT_TITLE = Utils.getAppId()+".ui.AddEditArtifactActivity.Artifact.title";
    public static String ARTIFACT_DESCRIPTION = Utils.getAppId()+".ui.AddEditArtifactActivity.Artifact.description";
    public static String ARTIFACT_LAT = Utils.getAppId()+".ui.AddEditArtifactActivity.Artifact.lat";
    public static String ARTIFACT_LON = Utils.getAppId()+".ui.AddEditArtifactActivity.Artifact.lon";

    /**
     * Activity mode (edit or create)
     */
    private String mode;

    /**
     * Artifacts repository
     */
    private ArtifactsRepository repository;

    /**
     * Title text editor
     */
    private EditText titleEditText;

    /**
     * Description text editor
     */
    private EditText descriptionEditText;

    /**
     * Artifact id
     */
    private int id;

    /**
     * Artifact latitude
     */
    private double latitude;

    /**
     * Artifact longitude
     */
    private double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_artifact);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //load repository
        ArtifactsDatabase database = Utils.getAppDatabase(this);
        this.repository = new ArtifactsRepository(this,database);

        //get mode
        this.mode = getIntent().getExtras().getString(MODE);

        //get gui elements
        this.titleEditText = (EditText) findViewById(R.id.edit_artifact_title);
        this.descriptionEditText = (EditText) findViewById(R.id.edit_artifact_description);

        //get position
        this.latitude = getIntent().getExtras().getDouble(ARTIFACT_LAT);
        this.longitude = getIntent().getExtras().getDouble(ARTIFACT_LON);

        //fill fields
        if(this.mode.equals(EDIT_ARTIFACT_MODE)){
            this.setTitle(getResources().getString(R.string.title_edit_artifact));

            this.id = getIntent().getExtras().getInt(ARTIFACT_ID);
            String title = getIntent().getExtras().getString(ARTIFACT_TITLE);
            String description = getIntent().getExtras().getString(ARTIFACT_DESCRIPTION);

            titleEditText.setText(title);
            descriptionEditText.setText(description);
        }else{
            this.setTitle(getResources().getString(R.string.title_add_artifact));
        }

        ImageView img = (ImageView)findViewById(R.id.artifact_image);
        img.setImageResource(R.drawable.img_default_placeholder);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_edit_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_confirm:
                String title = this.titleEditText.getText().toString();
                String description = this.descriptionEditText.getText().toString();
                double latitude = this.latitude;
                double longitude = this.longitude;

                if(validateField(title) == false){
                    Toast.makeText(this,"Invalid Title",Toast.LENGTH_LONG).show();
                    return false;
                }
                if(validateField(description) == false){
                    Toast.makeText(this,"Invalid Description",Toast.LENGTH_LONG).show();
                    return false;
                }

                if(this.mode.equals(EDIT_ARTIFACT_MODE)) {
                    this.repository.editArtifact(id,title,description);
                }else{
                    this.repository.newCreatedArtifact(title,description,latitude,longitude);
                }

                setResult(RESULT_OK);
                finish();
                return true;
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean validateField(String field){
        return field != null
                && !field.isEmpty()
                && !field.trim().equals("")
                && !field.contains(CommunicationProtocol.SEPARATOR_TOKEN)
                && !field.contains(String.valueOf(CommunicationProtocol.MESSAGE_DELIMITER_TOKEN))
                && !field.contains(CommunicationProtocol.MESSAGE_BODY_DELIMITER_TOKEN)
                && !field.contains(CommunicationProtocol.MESSAGE_VALUE_DELIMITER_TOKEN);
    }
}
