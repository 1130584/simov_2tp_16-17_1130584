package simov.artifacts.ui.ListPage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import simov.artifacts.R;
import simov.artifacts.model.Artifact;
import simov.artifacts.model.ArtifactViewModel;

/**
 * Created by Admin on 04/12/2016.
 *
 * Custom list adpater to show the a list of Artifacts
 */
public class ArtifactsListCustomListAdapter extends BaseAdapter {
    private ArrayList<ArtifactListItem> listData;
    private LayoutInflater layoutInflater;

    public ArtifactsListCustomListAdapter(Context context, ArrayList<ArtifactListItem> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    public void setData(ArrayList<ArtifactListItem> listData){
        this.listData = listData;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = layoutInflater.inflate(R.layout.artifacts_list_row_layout, null);

        ImageView image = (ImageView) rowView.findViewById(R.id.list_item_icon);
        TextView title = (TextView) rowView.findViewById(R.id.artifacts_list_row_title);
        TextView subtitle = (TextView) rowView.findViewById(R.id.artifacts_list_row_subtitle);

        if(listData.size() > 0 && listData != null){
            ArtifactListItem item = listData.get(position);
            if(item != null){

                switch (item.artifact.kind){
                    case CREATED:
                        image.setImageResource(R.drawable.artifact_created);
                        break;
                    case DISCOVERED:
                        image.setImageResource(R.drawable.artifact_discovered);
                        break;
                    case UNDISCOVERED:
                        image.setImageResource(R.drawable.artifact_undiscovered);
                        break;
                }
                title.setText(item.title);
                subtitle.setText(item.subtitle);
            }
        }

        return rowView;
    }

    public static class ArtifactListItem{

        public String title;
        public String subtitle;
        public ArtifactViewModel artifact;

        /**
         * Constructor
         */
        public ArtifactListItem(String title, String subtitle, ArtifactViewModel artifact){
            this.title = title;
            this.subtitle = subtitle;
            this.artifact = artifact;
        }
    }
}
