package simov.artifacts.ui.MainPage.Sections.StatsSection;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import simov.artifacts.R;

/**
 * Created by Admin on 04/12/2016.
 *
 * Custom list adpater to show the counts of the Artifacts in the stats page in a ListView
 */
public class StatsCustomListAdapter extends BaseAdapter {
    private ArrayList<StatsListItem> listData;
    private LayoutInflater layoutInflater;

    public StatsCustomListAdapter(Context context, ArrayList<StatsListItem> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    public void setData(ArrayList<StatsListItem> listData){
        this.listData = listData;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = layoutInflater.inflate(R.layout.stats_row_layout, null);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.list_item_icon);
        TextView leftText = (TextView) rowView.findViewById(R.id.stats_row_left_text);
        TextView rightText = (TextView) rowView.findViewById(R.id.stats_row_right_text);

        if(listData.size() > 0 && listData != null){
            StatsListItem item = listData.get(position);
            if(item != null){
                if(item.imageRecourceId != -1) {
                    imageView.setImageResource(item.imageRecourceId);
                }else{
                    imageView.setVisibility(View.INVISIBLE);
                }
                leftText.setText(item.leftText);
                rightText.setText(item.rightText);

                if(item.leftTextColorId != -1) {
                    leftText.setTextColor(item.leftTextColorId);
                }

                if(item.rightTextColorId != 1) {
                    rightText.setTextColor(item.rightTextColorId);
                }

                if(item.leftTextStyle != -1){
                    rightText.setTypeface(null,item.leftTextStyle);
                }

                if(item.rightTextStyle != -1){
                    rightText.setTypeface(null,item.rightTextStyle);
                }
            }
        }

        return rowView;
    }

    public static class StatsListItem{

        public int imageRecourceId;
        public String leftText;
        public String rightText;

        public int leftTextColorId = -1;
        public int rightTextColorId = -1;

        public int leftTextStyle = -1;
        public int rightTextStyle = -1;

        /**
         * Constructor
         */
        public StatsListItem(int imageRecourceId, String leftText,String rightText){
            this.imageRecourceId = imageRecourceId;
            this.leftText = leftText;
            this.rightText = rightText;
            this.rightTextColorId = Color.BLACK;
        }

        /**
         * Constructor
         */
        public StatsListItem(int imageRecourceId, String leftText, String rightText, int leftTextColorId, int leftTextStyle, int rightTextStyle){
            this(imageRecourceId,leftText,rightText);
            this.leftTextColorId = leftTextColorId;
            this.leftTextStyle = leftTextStyle;
            this.rightTextStyle = rightTextStyle;
        }
    }
}
