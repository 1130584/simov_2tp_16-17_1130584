package simov.artifacts.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import simov.artifacts.R;
import simov.artifacts.business.ArtifactsRepository;
import simov.artifacts.database.ArtifactsDatabase;
import simov.artifacts.model.Artifact;
import simov.artifacts.utils.Utils;

public class ViewArtifactActivity extends AppCompatActivity {

    private static final int EDIT_REQUEST_CODE = 1;
    public static final String ARTIFACT_ID = Utils.getAppId()+"ARTIFACT_ID";

    /**
     * Artifacts repository
     */
    private ArtifactsRepository repository;

    /**
     * Artifact
     */
    private Artifact artifact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_artifact);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //load repository
        ArtifactsDatabase database = Utils.getAppDatabase(this);
        this.repository = new ArtifactsRepository(this,database);

        int artifactId = getIntent().getExtras().getInt(ARTIFACT_ID);

        //setup view
        this.setup(artifactId);

        //setup image view
        ImageView img = (ImageView)findViewById(R.id.artifact_image);
        img.setImageResource(R.drawable.img_default_placeholder);
    }

    /**
     * Setup view
     * @param artifactId
     */
    private void setup(int artifactId){
        //get Artifact
        this.artifact = this.repository.getArtifact(artifactId);

        if(this.artifact != null){
            //set activity title
            setTitle(this.artifact.title);

            //set user
            TextView userTextView = (TextView)findViewById(R.id.view_artifact_user);
            userTextView.setText(this.artifact.userId != null ? this.artifact.userId : "Unknown User");

            //set title
            TextView titleTextView = (TextView)findViewById(R.id.view_artifact_title);
            titleTextView.setText(this.artifact.title);

            //set description
            TextView descriptionTextView = (TextView)findViewById(R.id.view_artifact_description);
            descriptionTextView.setText(this.artifact.description);
        }else{
            //set activity title
            setTitle(getResources().getString(R.string.artifact_not_found));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                this.setup(this.artifact.id);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(this.artifact.userId.equals(Utils.getUsername()) && this.artifact.serverId == 0) {
            getMenuInflater().inflate(R.menu.menu_preview_page, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_edit:
                Intent intent = new Intent(getBaseContext(),AddEditArtifactActivity.class);
                intent.putExtra(AddEditArtifactActivity.MODE,AddEditArtifactActivity.EDIT_ARTIFACT_MODE);
                intent.putExtra(AddEditArtifactActivity.ARTIFACT_ID,this.artifact.id);
                intent.putExtra(AddEditArtifactActivity.ARTIFACT_TITLE,this.artifact.title);
                intent.putExtra(AddEditArtifactActivity.ARTIFACT_DESCRIPTION,this.artifact.description);
                startActivityForResult(intent,EDIT_REQUEST_CODE);
                return true;
            case R.id.action_delete:

                new AlertDialog.Builder(this)
                        .setTitle("Delete Artifact")
                        .setMessage("Do you really want to delete the Artifact?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                if(repository.deleteArtifact(artifact.id)){
                                    Toast.makeText(getApplicationContext(),"Artifact deleted successfully",Toast.LENGTH_LONG).show();
                                    finish();
                                }else{
                                    Toast.makeText(getApplicationContext(),"Could not delete Artifact",Toast.LENGTH_LONG).show();
                                }
                            }})
                        .setNegativeButton(android.R.string.no, null).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
