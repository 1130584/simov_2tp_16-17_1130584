package simov.artifacts.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import simov.artifacts.R;
import simov.artifacts.business.ArtifactsRepository;
import simov.artifacts.database.ArtifactsDatabase;
import simov.artifacts.model.Artifact;
import simov.artifacts.utils.Utils;

public class SettingsActivity extends AppCompatActivity {

    public static final String PREFERENCES_NAME = Utils.getAppId()+".preferences";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Settings");

        SharedPreferences sharedpreferences = getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        boolean checked = sharedpreferences.getBoolean(getString(R.string.sh_pref_allow_artifical_location),false);

        ((CheckBox)findViewById(R.id.artifical_location_checkbox)).setChecked(checked);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onCheckboxClicked(View view) {
        boolean checked = ((CheckBox) view).isChecked();

        switch(view.getId()) {
            case R.id.artifical_location_checkbox:
                SharedPreferences sharedpreferences = getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean(getString(R.string.sh_pref_allow_artifical_location), checked);
                editor.commit();
                break;
        }
    }
}
