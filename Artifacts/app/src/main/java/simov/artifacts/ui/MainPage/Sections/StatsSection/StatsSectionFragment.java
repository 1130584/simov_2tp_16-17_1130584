package simov.artifacts.ui.MainPage.Sections.StatsSection;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import simov.artifacts.model.Artifact;
import simov.artifacts.model.ArtifactViewModel;
import simov.artifacts.ui.ListPage.ArtifactsListActivity;
import simov.artifacts.R;
import simov.artifacts.ui.ListPage.ArtifactsListCustomListAdapter;
import simov.artifacts.ui.MainPage.Sections.SectionFragment;

/**
 * Main page fragment for the Stats section
 */
public class StatsSectionFragment extends SectionFragment{

    //Item indexes
    public static final int ARTIFACTS_ACQUIRED = 0;
    public static final int ARTIFACTS_TO_ACQUIRE = 1;
    public static final int ARTIFACTS_CREATED = 2;
    public static final int ARTIFACTS_TOTAL = 3;

    //Item names
    public static final int[] ITEM_NAMES = {
        R.string.artifacts_acquired,
        R.string.artifacts_to_acquire,
        R.string.artifacts_created,
        R.string.artifacts_total
    };

    /**
     * Stats list adapter
     */
    private StatsCustomListAdapter statsListAdapter;

    //Artifacts retrieved from the database
    private List<ArtifactViewModel> artifactsAcquired;
    private List<ArtifactViewModel> artifactsToAcquire;
    private List<Artifact> artifactsCreated;
    private List<ArtifactViewModel> artifacts;

    /**
     * Constructor
     */
    public StatsSectionFragment() {
        super("Stats");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_stats, container, false);

        //setup stats for location Artifacts
        ListView locationArtifactsListView = (ListView) rootView.findViewById(R.id.location_artifacts_stats);

        //provide a temporary list
        final ArrayList<StatsCustomListAdapter.StatsListItem> locationArtifactsListData = new ArrayList<StatsCustomListAdapter.StatsListItem>();
        locationArtifactsListData.add(new StatsCustomListAdapter.StatsListItem(R.drawable.artifact_discovered,getResources().getString(ITEM_NAMES[ARTIFACTS_ACQUIRED]),"0"));
        locationArtifactsListData.add(new StatsCustomListAdapter.StatsListItem(R.drawable.artifact_undiscovered,getResources().getString(ITEM_NAMES[ARTIFACTS_TO_ACQUIRE]),"0"));
        locationArtifactsListData.add(new StatsCustomListAdapter.StatsListItem(R.drawable.artifact_created,getResources().getString(ITEM_NAMES[ARTIFACTS_CREATED]),"0"));
        locationArtifactsListData.add(new StatsCustomListAdapter.StatsListItem(-1,getResources().getString(ITEM_NAMES[ARTIFACTS_TOTAL]),"0", Color.parseColor("#026499"), Typeface.BOLD, Typeface.BOLD));

        // Assign adapter to ListView
        this.statsListAdapter = new StatsCustomListAdapter(this.getContext(), locationArtifactsListData);
        locationArtifactsListView.setAdapter(this.statsListAdapter);

        // ListView Item Click Listener
        locationArtifactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                StatsCustomListAdapter.StatsListItem item = (StatsCustomListAdapter.StatsListItem)statsListAdapter.getItem(position);
                if("".equals(item.rightText) || "0".equals(item.rightText)){
                    return;
                }

                //Goto an Activity that shows a list of Artifacts
                Intent intent = new Intent(getContext(), ArtifactsListActivity.class);
                intent.putExtra(ArtifactsListActivity.ARTIFACTS_TO_DISPLAY,position);
                intent.putExtra(ArtifactsListActivity.TITLE_TO_DISPLAY,getResources().getString(ITEM_NAMES[position]));
                startActivity(intent);
            }

        });

        //get data
        this.getArtifactsData();

        return rootView;
    }

    @Override
    public void onPauseFragment() {

    }

    @Override
    public void onResumeFragment() {
        this.getArtifactsData();
    }

    private void getArtifactsData(){
        //get counts
        int artifactsDiscovered = this.repository.getNumberOfArtifactsDiscovered();
        int artifactsToDiscover = this.repository.getNumberOfArtifactsToDiscover();
        int artifactsCreated = this.repository.getNumberOfArtifactsOfUser();
        int artifactsTotal = this.repository.getNumberOfArtifacts();

        //fill totals
        StatsCustomListAdapter.StatsListItem acquired = (StatsCustomListAdapter.StatsListItem)this.statsListAdapter.getItem(ARTIFACTS_ACQUIRED);
        acquired.rightText = ""+artifactsDiscovered;

        StatsCustomListAdapter.StatsListItem toAcquire = (StatsCustomListAdapter.StatsListItem)this.statsListAdapter.getItem(ARTIFACTS_TO_ACQUIRE);
        toAcquire.rightText = ""+artifactsToDiscover;

        StatsCustomListAdapter.StatsListItem created = (StatsCustomListAdapter.StatsListItem)this.statsListAdapter.getItem(ARTIFACTS_CREATED);
        created.rightText = ""+artifactsCreated;

        StatsCustomListAdapter.StatsListItem total = (StatsCustomListAdapter.StatsListItem)this.statsListAdapter.getItem(ARTIFACTS_TOTAL);
        total.rightText = ""+artifactsTotal;

        //Notify DataSet changed
        this.statsListAdapter.notifyDataSetChanged();
    }
}
