package simov.artifacts.ui.MainPage.Sections;

import android.support.v4.app.Fragment;

import simov.artifacts.business.ArtifactsRepository;
import simov.artifacts.database.ArtifactsLocalDatabase;

/**
 * Created by Admin on 16/12/2016.
 */

public abstract class SectionFragment extends Fragment{

    /**
     * Artifacts repository
     */
    protected ArtifactsRepository repository;

    /**
     * Title
     */
    private String title;

    /**
     * Constructor
     * @param title
     */
    public SectionFragment(String title){
        this.title = title;
    }

    /**
     * Set repository
     * @param repository
     */
    public void setRepository(ArtifactsRepository repository){
        this.repository = repository;
    }

    /**
     * Returns section title
     * @return
     */
    public String getTitle(){
        return this.title;
    }

    /**
     * On pause fragment
     */
    public abstract void onPauseFragment();

    /**
     * On resume fragment
     */
    public abstract void onResumeFragment();
}