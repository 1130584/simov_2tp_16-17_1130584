package simov.artifacts.business;

import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import simov.artifacts.alerts.ArtifactProximityAlertManager;
import simov.artifacts.communication.CommunicationProtocol;
import simov.artifacts.model.Artifact;
import simov.artifacts.utils.Utils;

/**
 * Created by Admin on 01/01/2017.
 */

public class ArtifactsSynchronization extends AsyncTask<Object, String, ArtifactsSynchronization.Result> {

    private static final String UNEXPECTED_ERROR = "Unexpected error. Synchronization finished";
    private static final String UNEXPECTED_CONNECTION_ERROR = "Unexpected connection error. Synchronization finished";

    /**
     * ArtifactsSynchronizationHostThreadInterface
     */
    private ArtifactsSynchronizationHostThreadInterface hostThread;

    /**
     * Bluetooth communication socket
     */
    private BluetoothSocket beaconSocket;

    /**
     * Repository
     */
    private ArtifactsRepository repository;

    /**
     * Context
     */
    private Context context;

    /**
     * Buffer I/O
     */
    private BufferedReader bufferReader;
    private BufferedWriter bufferWriter;


    /**
     * Constructor
     * @param beaconSocket
     */
    public ArtifactsSynchronization(Context context, ArtifactsSynchronizationHostThreadInterface hostThread, BluetoothSocket beaconSocket, ArtifactsRepository repository){
        this.context = context;
        this.hostThread = hostThread;
        this.beaconSocket = beaconSocket;
        this.repository = repository;
    }

    @Override
    protected Result doInBackground(Object... params) {
        boolean serverEndedConnection = false;
        Result result = new Result(0,0);

        //====== CREATE IO HANDLERS
        try {
            // prepare to receive data
            InputStream inputStream = this.beaconSocket.getInputStream();
            OutputStream outputStream = this.beaconSocket.getOutputStream();

            // create buffer reader/writer
            this.bufferReader = new BufferedReader(new InputStreamReader(inputStream));
            this.bufferWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
        }catch(Exception ex){
            result.errors.add(UNEXPECTED_CONNECTION_ERROR);
            return result;
        }

        try {
            String message;

            //====== CHECK SERVER AVAILABILITY

            publishProgress("Checking Beacon availability");

            //wait for server message of availability
            message = this.receiveMessage(bufferReader);
            if (CommunicationProtocol.DATABASE_UNAVAILABLE_MESSAGE.equals(message)) {
                result.errors.add("Beacon is unavailable at the moment");
                return result;
            } else if (CommunicationProtocol.DATABASE_AVAILABLE_MESSAGE.equals(message)) {

                publishProgress("Uploading Artifacts...");

                //============================
                //=== Send created Artifacts
                //============================

                this.send(bufferWriter, CommunicationProtocol.NEW_ARTIFACTS_BEGIN_MESSAGE);

                List<Artifact> artifacts = repository.getCreatedArtifacts();
                for (Artifact artifact : artifacts) {

                    //send created artifact
                    this.send(bufferWriter, CommunicationProtocol.NEW_ARTIFACT_MESSAGE
                            + artifact.userId + CommunicationProtocol.SEPARATOR_TOKEN
                            + artifact.title + CommunicationProtocol.SEPARATOR_TOKEN
                            + artifact.description + CommunicationProtocol.SEPARATOR_TOKEN
                            + artifact.latitude + CommunicationProtocol.SEPARATOR_TOKEN
                            + artifact.longitude);

                    //receive created artifact server ID
                    String reply = this.expectMessage(bufferReader, CommunicationProtocol.NEW_ARTIFACT_ID_MESSAGE, false);

                    //get server id
                    String id = this.getMessageProperty(reply, 0);
                    int serverId = Integer.parseInt(id);

                    //update artifact in database
                    if (this.repository.updateArtifactServerId(artifact.id, serverId)) {
                        result.artifactsUploaded++;
                        publishProgress("Uploading Artifacts... ( " + result.artifactsUploaded + " of " + artifacts.size() + " )");
                    } else {
                        throw new SynchronizationException("Artifact '" + artifact.title + "' failed to upload");
                    }
                }

                this.send(bufferWriter, CommunicationProtocol.NEW_ARTIFACTS_END_MESSAGE);

                ///============================
                //=== Download new Artifacts
                //============================

                publishProgress("Downloading Artifacts...");

                this.expectMessage(bufferReader, CommunicationProtocol.SERVER_ARTIFACTS_BEGIN_MESSAGE, true);

                //receive new Artifacts
                boolean finish = false;
                do {

                    //read artifact
                    message = this.receiveMessage(bufferReader);

                    if (message.startsWith(CommunicationProtocol.SERVER_ARTIFACT_MESSAGE)) {
                        //get artifact data
                        int serverId = Integer.parseInt(this.getMessageProperty(message, 0));
                        String user = this.getMessageProperty(message, 1);
                        String title = this.getMessageProperty(message, 2);
                        String description = this.getMessageProperty(message, 3);
                        String latitudeString = this.getMessageProperty(message, 4);
                        String longitudeString = this.getMessageProperty(message, 5);
                        double latitude = Double.parseDouble(latitudeString);
                        double longitude = Double.parseDouble(longitudeString);

                        //store artifact in database
                        if (!this.repository.hasArtifact(serverId)) {
                            if (this.repository.newArtifact(user, serverId, title, description, latitude, longitude)) {
                                //add proximity alert to allow user to discover this Artifact when he is near it
                                Artifact artifact = this.repository.getArtifactByServerId(serverId);
                                ArtifactProximityAlertManager.createProximityAlertForArtifact(this.context, artifact);

                                //publish progress
                                result.artifactsDownloaded++;
                                publishProgress("Downloading Artifacts... ( " + result.artifactsDownloaded + " )");
                            }
                        }

                    } else if (CommunicationProtocol.SERVER_ARTIFACTS_END_MESSAGE.equals(message)) {
                        finish = true;
                    } else {
                        throw new SynchronizationException(UNEXPECTED_CONNECTION_ERROR);
                    }

                } while (finish == false);
            }else{
                result.errors.add(UNEXPECTED_ERROR);
            }
        }catch(IOException | NumberFormatException ex){
            result.errors.add(UNEXPECTED_ERROR);
        }catch(SynchronizationException ex){
            result.errors.add(ex.getMessage());
        }catch(EndConnectionException ex){
            serverEndedConnection = true;
        }

        try{
            if(serverEndedConnection == false) {
                //close connection
                this.send(bufferWriter, CommunicationProtocol.APP_END_CONNECTION_MESSAGE);
            }

            bufferWriter.close();
            bufferReader.close();
        }catch(Exception ex){

        }

        return result;
    }

    @Override
    protected void onPreExecute() {
        this.hostThread.onSynchronizationStart();
    }

    @Override
    protected void onProgressUpdate(String... progress) {
        this.hostThread.onSynchronizationProgressUpdate(progress[0]);
    }

    @Override
    protected void onPostExecute(Result result) {
        this.hostThread.onSynchronizationFinish(result);
    }

    protected void onCancelled(Result result) {
        try {
            if(bufferWriter != null && bufferReader != null) {
                //close connection
                this.send(bufferWriter, CommunicationProtocol.APP_END_CONNECTION_MESSAGE);

                bufferReader.close();
                bufferWriter.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Wait for a message from client
     * @param bufferReader
     * @return
     * @throws Exception
     */
    private String receiveMessage(BufferedReader bufferReader) throws EndConnectionException, IOException{
        String lineRead = this.readUntilCharacter(bufferReader,CommunicationProtocol.MESSAGE_DELIMITER_TOKEN);

        if(CommunicationProtocol.SERVER_END_CONNECTION_MESSAGE.equals(lineRead)){
            throw new EndConnectionException();
        }

        return lineRead;
    }

    /**
     * Wair fot a specific message from client
     */
    private String expectMessage(BufferedReader bufferReader, String message, boolean exactMessage) throws SynchronizationException, EndConnectionException, IOException{
        String lineRead = this.readUntilCharacter(bufferReader,CommunicationProtocol.MESSAGE_DELIMITER_TOKEN);

        if(CommunicationProtocol.SERVER_END_CONNECTION_MESSAGE.equals(lineRead)){
            throw new EndConnectionException();
        }

        if(exactMessage == true){
            if(message.equals(lineRead)){
                return message;
            }
        }else{
            if(lineRead.startsWith(message)){
                return lineRead;
            }
        }

        throw new SynchronizationException(UNEXPECTED_CONNECTION_ERROR);
    }

    /**
     * Send message to client
     */
    private void send(BufferedWriter writer, String message) throws IOException{
        writer.write(message);
        writer.write(CommunicationProtocol.MESSAGE_DELIMITER_TOKEN);
        writer.flush();
    }

    /**
     * Get property from a complex message
     * @param message
     * @param index
     * @return
     */
    private String getMessageProperty(String message, int index){
        String[] messageMainParts = message.split(CommunicationProtocol.MESSAGE_VALUE_DELIMITER_TOKEN);

        if(messageMainParts.length != 2){
            return null;
        }

        String[] messageParts = messageMainParts[1].split(CommunicationProtocol.SEPARATOR_TOKEN);

        if(messageParts.length > index){
            return messageParts[index];
        }

        return null;
    }

    /**
     * Reads from buffer until a certain character is readed
     */
    private String readUntilCharacter(BufferedReader bufferReader, char character) throws IOException{
        StringBuilder builder = new StringBuilder();
        char characterReaded = '0';

        do{
            characterReaded = (char)bufferReader.read();
            if(characterReaded != character){
                builder.append(characterReaded);
            }
        }while(characterReaded != character);

        return builder.toString();
    }

    /**
     * Result information object
     */
    public class Result{

        public int artifactsDownloaded;
        public int artifactsUploaded;
        public List<String> errors;

        public Result(int artifactsDownloaded,int artifactsUploaded){
            this.artifactsDownloaded = artifactsDownloaded;
            this.artifactsUploaded = artifactsUploaded;
            this.errors = new ArrayList<>();
        }
    }

    /**
     * Synchronization exception
     */
    private class SynchronizationException extends Exception{

        /**
         * Constructor
         * @param message
         */
        public SynchronizationException(String message){
            super(message);
        }
    }

    /**
     * EndConnectionException
     */
    private class EndConnectionException extends Exception{
        public EndConnectionException(){
            super("Synchronization terminated");
        }
    }
}

