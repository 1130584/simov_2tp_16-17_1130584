package simov.artifacts.business;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import simov.artifacts.database.ArtifactsDatabase;
import simov.artifacts.model.Artifact;
import simov.artifacts.model.ArtifactDiscovered;
import simov.artifacts.model.ArtifactViewModel;
import simov.artifacts.utils.Utils;

/**
 * Created by Admin on 17/12/2016.
 */

public class ArtifactsRepository {

    private static final String USERNAME_NOT_AVAILABLE = "Device google account name not available";

    /**
     * Artifacts Database
     */
    private ArtifactsDatabase database;

    /**
     * Context
     */
    private Context context;

    /**
     * Constructor
     * @param database
     */
    public ArtifactsRepository(Context context, ArtifactsDatabase database){
        this.context = context;
        this.database = database;
    }

    /**
     * Set Artifacts Database
     * @param database
     */
    public void setDatabase(ArtifactsDatabase database){
        this.database = database;
    }

    /**
     * New Created Artifact
     * @param title
     * @param description
     * @param latitude
     * @param longitude
     * @return
     */
    public boolean newCreatedArtifact(String title, String description, double latitude, double longitude){
        String username = Utils.getUsername();
        if(username == null){
            Toast.makeText(this.context,USERNAME_NOT_AVAILABLE,Toast.LENGTH_LONG).show();
            return false;
        }

        return this.database.newArtifact(Utils.getUsername(), 0, title,description,latitude,longitude);
    }

    /**
     * New Artifact
     * @param userId
     * @param serverId
     * @param title
     * @param description
     * @param latitude
     * @param longitude
     * @return
     */
    public boolean newArtifact(String userId, int serverId, String title, String description, double latitude, double longitude){
        return this.database.newArtifact(userId, serverId, title,description,latitude,longitude);
    }

    /**
     * Update Artifact server id
     * @param artifactId
     * @param serverId
     * @return
     */
    public boolean updateArtifactServerId(int artifactId, int serverId){
        return this.database.updateArtifactServerId(artifactId,serverId);
    }

    /**
     * New Artifact
     * @param title
     * @param description
     * @return
     */
    public boolean editArtifact(int id, String title, String description){
        String username = Utils.getUsername();
        if(username == null){
            Toast.makeText(this.context,USERNAME_NOT_AVAILABLE,Toast.LENGTH_LONG).show();
            return false;
        }

        Artifact artifact = this.database.getArtifact(id);
        if(artifact != null && artifact.userId.equalsIgnoreCase(username) && artifact.serverId == 0){
            return this.database.editArtifact(id,title,description);
        }
        return false;
    }

    /**
     * Delete Artifact
     * @param id
     * @return
     */
    public boolean deleteArtifact(int id){
        String username = Utils.getUsername();
        if(username == null){
            Toast.makeText(this.context,USERNAME_NOT_AVAILABLE,Toast.LENGTH_LONG).show();
            return false;
        }

        Artifact artifact = this.database.getArtifact(id);
        if(artifact != null && artifact.userId.equals(username) && artifact.serverId == 0){
            return this.database.deleteArtifact(id);
        }
        return false;
    }

    /**
     * If database already has a specific artifact
     * @param serverId
     * @return
     */
    public boolean hasArtifact(int serverId){
        return this.database.hasArtifact(serverId);
    }

    /**
     * New Discovered Artifact
     * @param databaseId
     * @return
     */
    public Artifact newArtifactDiscovered(int databaseId){
        Artifact artifact = this.database.getArtifactByServerId(databaseId);
        if(artifact != null){
            if(this.database.newArtifactDiscovered(databaseId)){
                return artifact;
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    /**
     * Returns an Artifact by its id
     * @param id
     * @return
     */
    public Artifact getArtifact(int id){
        return this.database.getArtifact(id);
    }

    /**
     * Returns an Artifact by its server id
     * @param id
     * @return
     */
    public Artifact getArtifactByServerId(int id){
        return this.database.getArtifactByServerId(id);
    }

    /**
     * Returns all Artifacts
     * @param getDiscovered if discovered Artifacts go in the results
     * @param getUndiscovered if undiscovered Artifacts go in the results
     * @param getCreated if created Artifacts go in the results
     * @return a list of ArtifactViewModel
     */
    public List<ArtifactViewModel> getAllArtifacts(boolean getDiscovered, boolean getUndiscovered, boolean getCreated){
        String username = Utils.getUsername();
        if(username == null){
            Toast.makeText(this.context,USERNAME_NOT_AVAILABLE,Toast.LENGTH_LONG).show();
            return new ArrayList<>();
        }

        List<ArtifactViewModel> result = new ArrayList<>();

        //get all artifacts
        List<Artifact> artifacts = this.database.getAllArtifacts();

        //get all artifacts discovered
        List<ArtifactDiscovered> artifactsDiscovered = this.database.getAllArtifactsDiscovered();

        //fill artifacts array
        for(Artifact artifact : artifacts){
            boolean wasCreatedByUser = false;
            boolean isDiscovered = false;
            String dateDiscovered = null;

            //check if Artifact was created by user
            if(username.equals(artifact.userId)){
                wasCreatedByUser = true;
            }

            //check if Artifact was discovered by the user
            if(wasCreatedByUser == false) {
                for (ArtifactDiscovered artifactDiscovered : artifactsDiscovered) {
                    if (artifact.serverId == artifactDiscovered.artifactId) {
                        isDiscovered = true;
                        dateDiscovered = artifactDiscovered.date;
                        break;
                    }
                }
            }

            //add artifact to results
            if(isDiscovered == true){
                if(getDiscovered == true){
                    result.add(new ArtifactViewModel(dateDiscovered, ArtifactViewModel.ArtifactKind.DISCOVERED, artifact));
                }
            }else{
                if(wasCreatedByUser){
                    if (getCreated == true) {
                        result.add(new ArtifactViewModel(null, ArtifactViewModel.ArtifactKind.CREATED, artifact));
                    }
                }else {
                    if (getUndiscovered == true) {
                        result.add(new ArtifactViewModel(null, ArtifactViewModel.ArtifactKind.UNDISCOVERED, artifact));
                    }
                }
            }

        }

        return result;
    }

    /**
     * Get Artifacts of the user
     * @return
     */
    public List<Artifact> getUserArtifacts(){
        String username = Utils.getUsername();
        if(username == null){
            Toast.makeText(this.context,USERNAME_NOT_AVAILABLE,Toast.LENGTH_LONG).show();
            return new ArrayList<>();
        }

        return this.database.getAllArtifactsOfUser(username);
    }

    /**
     * Get all Artifacts created by the user
     * @return
     */
    public List<Artifact> getCreatedArtifacts(){
        String username = Utils.getUsername();
        if(username == null){
            Toast.makeText(this.context,USERNAME_NOT_AVAILABLE,Toast.LENGTH_LONG).show();
            return new ArrayList<>();
        }

        return this.database.getAllArtifactsCreated(username);
    }

    /**
     * Returns the number of Artifacts
     * @return
     */
    public int getNumberOfArtifacts(){
        return this.database.getAllArtifactsCount();
    }

    /**
     * Returns the number of Artifacts created
     * @return
     */
    public int getNumberOfArtifactsCreated(){
        String username = Utils.getUsername();
        if(username == null){
            Toast.makeText(this.context,USERNAME_NOT_AVAILABLE,Toast.LENGTH_LONG).show();
            return 0;
        }

        return this.database.getAllArtifactsCreatedCount(username);
    }

    /**
     * Returns the number of Artifacts of user
     * @return
     */
    public int getNumberOfArtifactsOfUser(){
        String username = Utils.getUsername();
        if(username == null){
            Toast.makeText(this.context,USERNAME_NOT_AVAILABLE,Toast.LENGTH_LONG).show();
            return 0;
        }

        return this.database.getAllArtifactsOfUserCount(username);
    }

    /**
     * Returns the number of Artifacts discovered
     * @return
     */
    public int getNumberOfArtifactsDiscovered(){
        return this.database.getAllArtifactsDiscoveredCount();
    }

    /**
     * Returns the number of Artifacts to discover
     * @return
     */
    public int getNumberOfArtifactsToDiscover(){
        return this.getNumberOfArtifacts() - (this.getNumberOfArtifactsOfUser() + this.getNumberOfArtifactsDiscovered());
    }
}
