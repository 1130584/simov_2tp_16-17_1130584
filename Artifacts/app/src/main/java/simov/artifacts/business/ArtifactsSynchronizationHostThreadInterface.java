package simov.artifacts.business;

/**
 * Created by Admin on 01/01/2017.
 */
public interface ArtifactsSynchronizationHostThreadInterface {

    /**
     * When synchronization starts
     */
    void onSynchronizationStart();

    /**
     * When synchronization updates progress
     * @param progress
     */
    void onSynchronizationProgressUpdate(String progress);

    /**
     * When synchronization finishes
     * @param result
     */
    void onSynchronizationFinish(ArtifactsSynchronization.Result result);
}
