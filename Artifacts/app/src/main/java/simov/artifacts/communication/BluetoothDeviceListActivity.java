package simov.artifacts.communication;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by Admin on 31/12/2016.
 */

public abstract class BluetoothDeviceListActivity extends AppCompatActivity{

    /**
     * New device found
     * @param name
     */
    public abstract void newDeviceFound(String name);

    /**
     * Bluetooth discovery started
     */
    public abstract void bluetoothDiscoveryStarted();

    /**
     * Bluetooth discovery finished
     */
    public abstract void bluetoothDiscoveryFinished();

    /**
     * When bluetooth state changed
     * @param state
     */
    public abstract void bluetoothStateChanged(int state);
}
