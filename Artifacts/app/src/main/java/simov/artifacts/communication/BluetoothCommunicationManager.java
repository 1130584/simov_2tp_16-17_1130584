package simov.artifacts.communication;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.Toast;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Admin on 31/12/2016.
 */

public class BluetoothCommunicationManager {

    /**
     * Bluetooth adapter
     */
    private BluetoothAdapter bluetoothAdapter;

    /**
     * Devices found in the discovery process
     */
    private Map<String,BluetoothDevice> devicesFound = new HashMap<String,BluetoothDevice>();

    /**
     * Constructor
     */
    public BluetoothCommunicationManager(final BluetoothDeviceListActivity activity) throws Exception {
        this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if(this.bluetoothAdapter == null){
            throw new Exception("Bluetooth not supported");
        }

        BroadcastReceiver bluetoothStateMonitor = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                int state = intent.getExtras().getInt(BluetoothAdapter.EXTRA_STATE);

                activity.bluetoothStateChanged(state);
            }
        };

        //register state monitor
        activity.registerReceiver(bluetoothStateMonitor,new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
    }

    /**
     * Cancel discovery
     */
    public void stopDiscoverDevices(){
        this.bluetoothAdapter.cancelDiscovery();
    }

    /**
     * Start the device discovery
     */
    public void startDiscoverDevices(final BluetoothDeviceListActivity activity){
        if(!this.bluetoothAdapter.isEnabled()){
            return;
        }

        if(this.bluetoothAdapter.isDiscovering()){
            return;
        }

        //create broadcast receivers
        BroadcastReceiver discoveryMonitor = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(intent.getAction())) {

                    // Discovery has completed.
                    activity.bluetoothDiscoveryFinished();
                }
                else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(intent.getAction())) {

                    // Discovery has completed.
                    activity.bluetoothDiscoveryStarted();
                }
                else if (BluetoothDevice.ACTION_FOUND.equals(intent.getAction())) {

                    //get device properties
                    String remoteDeviceName = intent.getStringExtra(BluetoothDevice.EXTRA_NAME);
                    BluetoothDevice remoteDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                    if(devicesFound.get(remoteDeviceName) == null) {
                        //add device
                        devicesFound.put(remoteDeviceName, remoteDevice);

                        //notify listener
                        activity.newDeviceFound(remoteDeviceName);
                    }
                }
            }
        };

        //clear previously found devices
        this.devicesFound = new HashMap<>();

        //register receiver
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        activity.registerReceiver(discoveryMonitor,filter);

        //start discovery
        this.bluetoothAdapter.startDiscovery();
    }

    /**
     * Attemps to connect to a device found
     * If device is not an official Artifacts synchronization beacon it will return null
     * otherwise returns the socket for communication
     * @param deviceName
     */
    public BluetoothSocket connectWithBeaconDevice(String deviceName){
        try {
            BluetoothDevice device = this.devicesFound.get(deviceName);

            if(device != null) {

                UUID uuid = UUID.fromString(CommunicationProtocol.ARTIFACTS_SYNC_BEACON_UUID_STRING);
                BluetoothSocket clientSocket = device.createRfcommSocketToServiceRecord(uuid);

                if(clientSocket != null) {
                    clientSocket.connect();
                }
                return clientSocket;
            }else{
                return null;
            }
        }catch (Exception ex){
            return null;
        }
    }

    /**
     * Turn on bluetooth
     * @param activity
     */
    public void turnBluetoothOn(BluetoothDeviceListActivity activity){
        if (!this.bluetoothAdapter.isEnabled()) {
            Intent turnOnBluetoothActivity = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivity(turnOnBluetoothActivity);
        }
    }

    /**
     * Turn of bluetooth
     * @param activity
     */
    public void turnBluetoothOff(BluetoothDeviceListActivity activity){
        this.bluetoothAdapter.disable();
        Toast.makeText(activity, "Bluetooth turned off" ,Toast.LENGTH_LONG).show();
    }
}
