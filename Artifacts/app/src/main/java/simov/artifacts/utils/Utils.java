package simov.artifacts.utils;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import simov.artifacts.database.ArtifactsDatabase;
import simov.artifacts.database.ArtifactsLocalDatabase;

/**
 * Created by Admin on 17/12/2016.
 */

public class Utils {

    /**
     * Database
     */
    private static ArtifactsDatabase database;

    /**
     * Username
     */
    private static String username;

    /**
     * Sets the username
     */
    public static void setUsername(String newUsername){
        if(username == null) {
            username = newUsername;
        }
    }

    /**
     * Returns the username
     * @return
     */
    public static String getUsername(){
        return username;
    }

    /**
     * Returns the application database
     * @param context
     * @return
     */
    public static ArtifactsDatabase getAppDatabase(Context context){
        if(database != null){
            return database;
        }else{
            database = new ArtifactsLocalDatabase(context);
            return database;
        }
    }

    /**
     * Returns app id
     * @return
     */
    public static String getAppId(){
        return "simov.artifacts";
    }

    /**
     * Get current date
     * @return
     */
    public static String getCurrentDate(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("hh:mm dd-MM-yyyy");
        return df.format(calendar.getTime());
    }
}
