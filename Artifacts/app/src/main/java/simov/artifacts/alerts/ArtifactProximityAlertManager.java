package simov.artifacts.alerts;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;

import simov.artifacts.R;
import simov.artifacts.business.ArtifactsRepository;
import simov.artifacts.database.ArtifactsDatabase;
import simov.artifacts.model.Artifact;
import simov.artifacts.ui.MainPage.MainPageActivity;
import simov.artifacts.utils.Utils;

/**
 * Created by Admin on 21/12/2016.
 */

public class ArtifactProximityAlertManager extends BroadcastReceiver {

    /**
    * Artifact proximity alert intent key
    */
    private static final String ARTIFACT_PROXIMITY_ALERT = "simov.artifacts.alerts.ArtifactProximityAlertManager";
    private static final String ARTIFACT_PROXIMITY_ALERT_ARTIFACT_SERVER_ID = ARTIFACT_PROXIMITY_ALERT + ".Artifact.server_id";

    /**
     * Proximity alert radius
     */
    private static final float PROXIMITY_ALERT_RADIUS = 100f; //100 meters

    /**
     * Creates a proximity alert for an Artifact
     * @param context
     * @param artifact
     * @return
     */
    public static boolean createProximityAlertForArtifact(Context context, Artifact artifact){
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        //Create intent
        Intent intent = new Intent(ARTIFACT_PROXIMITY_ALERT);
        intent.putExtra(ARTIFACT_PROXIMITY_ALERT_ARTIFACT_SERVER_ID,artifact.serverId);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, -1, intent, 0);

        try {
            //create proximity alert
            locationManager.addProximityAlert(artifact.latitude, artifact.longitude, PROXIMITY_ALERT_RADIUS, -1, pendingIntent);
            return true;
        }catch (SecurityException ex){
            return false;
        }
    }

    @Override
    public void onReceive(Context context, Intent arg1) {
        int artifactServerId = arg1.getExtras().getInt(ARTIFACT_PROXIMITY_ALERT_ARTIFACT_SERVER_ID);

        if(artifactServerId <= 0){
            return;
        }

        //load repository
        ArtifactsDatabase database = Utils.getAppDatabase(context);
        ArtifactsRepository repository = new ArtifactsRepository(context,database);

        //get artifact
        Artifact artifact = repository.getArtifactByServerId(artifactServerId);
        if(artifact == null){
            return;
        }

        //register artifact discovered
        Artifact artifactDiscovered = repository.newArtifactDiscovered(artifactServerId);

        //get notification manager
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        //create intent that calls the main activity
        Intent i = new Intent(context, MainPageActivity.class);
        i.putExtra(MainPageActivity.INITIAL_LOCATION, true);
        i.putExtra(MainPageActivity.ARTIFACT_FOUND_LATITUDE, artifact.latitude);
        i.putExtra(MainPageActivity.ARTIFACT_FOUND_LATITUDE, artifact.longitude);
        PendingIntent pIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), i, 0);


        // build notification
        Notification notification = new Notification.Builder(context)
                .setContentTitle("New Artifact discovered!")
                .setContentText(artifact.title)
                .setSmallIcon(R.drawable.ic_location)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .build();

        //fire notification
        notificationManager.notify(0, notification);
    }
}
