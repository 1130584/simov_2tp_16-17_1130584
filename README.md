"# Artifacts android application README" 

Proposta:

Para o trabalho prático pretendemos desenvolver um sistema constituído por uma aplicação Android, 
e um conjunto de outra(s) pequena(s) aplicação(ões) auxiliar(s) noutro(s) formato(s) e plataforma. Ainda não pensamos num nome, mas este irá surgir brevemente.

O produto final irá consistir num sistema de partilha de elementos com informação(ideias, memórias, eventos, acontecimentos, pontos de interesse, etc..) num formato com os seguintes elementos: nome, texto descritivo e imagem(op.). 
Cada elemento, que decidimos denominar de "Artefacto", pode ter duas categorias que correspondem ao modo como são partilhados. Ou seja, podem ser Artefactos temporais ou espaciais, sendo que os temporais são partilhados numa data específica do futuro e os espaciais num local específico do mundo. 

Cada utilizador pode partilhar Artefactos com os outros utilizadores e também pode visualizar os Artefactos partilhados pelos mesmos. 

Visualização de artefactos:
- Temporais: O utilizador só os pode visualizar/desbloquear quando o tempo atingir a data à qual estes estão associados.
- Espaciais: O utilizador só os pode visualizar/desbloquear quando se deslocar para perto do local onde estes foram partilhados (necessidade do sensor GPS activo)

Partilha de Artefactos:
- Temporais: O utilizador pode partilhar um Artefacto desta categoria em qualquer altura mas estes tem de ser associados a uma data do futuro.
- Espaciais: O utilizador só pode partilhar um Artefacto desta categoria quando estiver no local que o pretende partilhar (necessidade do sensor GPS activo)

Sincronização da informação entre utilizadores:
Como os requisitos deste trabalho incluíam o uso dos diversos sensores/serviços disponíveis num sistema móvel e os quais trazem vantagem em relação ao sistemas fixos, 
decidimos usar um método de sincronização diferente do comum para os utilizadores partilharem e receberem Artefactos com e de outros utilizadores.
Este método consiste na utilização de "Hotspots" pela aplicação, que fornecem os serviços de sincronização dos Artefactos . Estes Hotspots podem ser acedidos através de Bluetooth ou Wi-Fi Direct.

Basicamente, o utilizador poderá criar Artefactos offline (onde são armazenados temporariamente no smartphone) e quando encontrar um Hotspot ligar-se ao mesmo 
(dependendo do tipo de Hotspot: com bluetooth ou Wi-Fi Direct), podendo sincronizar os artefactos, depositar os novos que criou ou descarregar os que os outros utilizadores depositaram. Podemos denominar este sistema como "banco" de artefactos. Todos os Hotspots que estão espalhados pelo mundo estariam, na teoria, sincronizados entre eles. Isto é, tudo o que fosse depositado num Hotspot seria replicado para os outros.

No entanto, como é lógico, iremos focar-nos na aplicação Android, sendo o restante desenvolvido de modo simplificado apenas para efeitos de simulação. 
Por isso, iremos desenvolver uma simples aplicação servidora externa para efectuar a funcionalidade do Hotspot, o qual será simulado usando um computador com placa de Bluetooth e placa de rede Wireless.

Este trabalho irá utilizar os seguintes serviços disponíveis no Android:

- Base de dados local
- WiFi e Dados Móveis
- Bluetooth
- Localização GPS
- Acelerómetro (ainda por investigar a necessidade)